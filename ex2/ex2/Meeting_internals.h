
#ifndef MEETING_INTERNALS_HEADER
#define MEETING_INTERNALS_HEADER

template <typename TimeType>
Meeting_t<TimeType>::Meeting_t(IN const std::string& subject, IN const TimeType& startTime, IN const TimeType& endTime) :
	m_subject(subject),
	m_startTime(startTime),
	m_endTime(endTime)
{
	verifyTimes();
}


template <typename TimeType>
Meeting_t<TimeType>::~Meeting_t()
{
	// Nothing to do here
}

template <typename TimeType>
Meeting_t<TimeType>::Meeting_t(IN const Meeting_t& other) :
	m_subject(other.m_subject),
	m_startTime(other.m_startTime),
	m_endTime(other.m_endTime)
{
	// Nothing to do here
}

template <typename TimeType>
Meeting_t<TimeType>& Meeting_t<TimeType>::operator=(IN const Meeting_t& other)
{
	m_subject = other.m_subject;
	m_startTime = other.m_startTime;
	m_endTime = other.m_endTime;

	return *this;
}

template <typename TimeType>
const std::string& Meeting_t<TimeType>::getSubject() const
{
	return m_subject;
}

template <typename TimeType>
const TimeType& Meeting_t<TimeType>::getStartTime() const
{
	return m_startTime;
}

template <typename TimeType>
const TimeType& Meeting_t<TimeType>::getEndTime() const
{
	return m_endTime;
}

template <typename TimeType>
bool Meeting_t<TimeType>::operator==(IN const Meeting_t& other) const
{
	if (m_endTime <= other.m_startTime)
	{
		return false;
	}

	if (m_startTime >= other.m_endTime)
	{
		return false;
	}

	return true;
}

template <typename TimeType>
void Meeting_t<TimeType>::print() const
{
	std::cout << "Subject: " << m_subject << std::endl;
	std::cout << "Start time: " << m_startTime << std::endl;
	std::cout << "End time: " << m_endTime << std::endl;
}

template <typename TimeType>
void Meeting_t<TimeType>::verifyTimes() const
{
	verifyTimeRange(m_startTime);
	verifyTimeRange(m_endTime);

	if (m_startTime > m_endTime)
	{
		throw std::invalid_argument("Start time must be smaller than end time");
	}
}

template <typename TimeType>
void Meeting_t<TimeType>::verifyTimeRange(IN const TimeType& time) const
{
	if ((0 > time) || (24 <= time))
	{
		throw std::invalid_argument("time must be between 0 (including) and 24 (not including)");
	}
}

#endif
