
#ifndef MEETING_WITH_PLACE_INTERNALS_HEADER
#define MEETING_WITH_PLACE_INTERNALS_HEADER

template <typename TimeType>
MeetingWithPlace_t<TimeType>::MeetingWithPlace_t(
	IN const std::string& subject,
	IN const TimeType& startTime,
	IN const TimeType& endTime,
	IN const std::string& place) :

	Meeting_t<TimeType>(subject, startTime, endTime),
	m_place(place)
{
	// Nothing to do here
}

template <typename TimeType>
MeetingWithPlace_t<TimeType>::~MeetingWithPlace_t()
{
	// Nothing to do here
}

template <typename TimeType>
MeetingWithPlace_t<TimeType>::MeetingWithPlace_t(IN const MeetingWithPlace_t& other) :
	Meeting_t<TimeType>(other), 
	m_place(other.m_place)
{
	// Nothing to do here
}

template <typename TimeType>
MeetingWithPlace_t<TimeType>& MeetingWithPlace_t<TimeType>::operator=(IN const MeetingWithPlace_t& other)
{
	Meeting_t::operator=(other);
	m_place = other.m_place;

	return *this;
}

template <typename TimeType>
const std::string& MeetingWithPlace_t<TimeType>::getPlace() const
{
	return m_place;
}

template <typename TimeType>
void MeetingWithPlace_t<TimeType>::print() const
{
	Meeting_t::print();
	
	std::cout << "Place: " << m_place << std::endl;
}

#endif
