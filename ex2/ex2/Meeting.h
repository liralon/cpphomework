
#ifndef MEETING_HEADER
#define MEETING_HEADER

#include "Basic.h"

template <typename TimeType>
class Meeting_t
{

public:

	typedef typename TimeType TimeType;

	inline Meeting_t(IN const std::string& subject, IN const TimeType& startTime, IN const TimeType& endTime);

	virtual ~Meeting_t();

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(Meeting_t);

	inline const std::string& getSubject() const;
	inline const TimeType& getStartTime() const;
	inline const TimeType& getEndTime() const;

	bool operator==(IN const Meeting_t& other) const;

	virtual void print() const;

protected:

	void verifyTimes() const;
	void verifyTimeRange(IN const TimeType& time) const;

private:
	std::string m_subject;
	TimeType m_startTime;
	TimeType m_endTime;

};

#include "Meeting_internals.h"

#endif
