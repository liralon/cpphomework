
#include "Basic.h"
#include "DayCalender.h"
#include "MeetingWithPlace.h"

// Note - Change the template parameter according to the version you want to test (They should behave the same)
typedef unsigned int SelectedTimeType;
//typedef float SelectedTimeType;

typedef DayCalender_t<SelectedTimeType> DayCalender;
typedef Meeting_t<SelectedTimeType> Meeting;
typedef MeetingWithPlace_t<SelectedTimeType> MeetingWithPlace;

typedef void(*CommandHandler)(IN OUT DayCalender& calender);

struct Command
{
	CommandHandler handler;
	std::string description;
};

static unsigned int GetIntegerFromUser(IN OUT std::ostream& os, OUT std::istream& is)
{
	unsigned int integer = 0;
	is >> integer;
	while (!is.good())
	{
		os << "Invalid integer. Please retry:" << std::endl;
		is.clear();
		is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		is >> integer;
	}

	return integer;
}

void GetMeetingParameters(OUT std::string& subject, OUT Meeting::TimeType& startTime, OUT Meeting::TimeType& endTime)
{
	std::cout << "Enter subject: ";
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::getline(std::cin, subject);

	std::cout << "Enter start time: ";
	std::cin >> startTime;

	std::cout << "Enter end time: ";
	std::cin >> endTime;
	
	while (!std::cin.good())
	{
		std::cout << std::endl << "Invalid times input. Please retry:" << std::endl;
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		std::cout << "Enter start time: ";
		std::cin >> startTime;

		std::cout << "Enter end time: ";
		std::cin >> endTime;
	}
}

void GetMeetingWithPlaceParameters(OUT std::string& subject, OUT Meeting::TimeType& startTime, OUT Meeting::TimeType& endTime, OUT std::string& place)
{
	GetMeetingParameters(subject, startTime, endTime);

	std::cout << "Enter place: ";
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::getline(std::cin, place);
}

Meeting* const GetMeetingFromUser()
{
	std::cout << "Does the meeting have a place? (0 for no or another integer for yes): ";
	const unsigned int doesMeetingHavePlace = GetIntegerFromUser(std::cout, std::cin);

	std::string subject;
	Meeting::TimeType startTime;
	Meeting::TimeType endTime;
	if (doesMeetingHavePlace)
	{
		std::string place;
		GetMeetingWithPlaceParameters(subject, startTime, endTime, place);
		return new MeetingWithPlace(subject, startTime, endTime, place);
	}
	else
	{
		GetMeetingParameters(subject, startTime, endTime);
		return new Meeting(subject, startTime, endTime);
	}
}

void AddMeeting(IN OUT DayCalender& calender)
{
	Meeting* const meeting = GetMeetingFromUser();
	calender.add(meeting);
}

void RemoveMeeting(IN OUT DayCalender& calender)
{
	std::cout << "Enter start time of meeting to remove: ";
	DayCalender::TimeType startTime;
	std::cin >> startTime;

	const Meeting* const removedMeeting = calender.remove(startTime);
	if (NULL != removedMeeting)
	{
		std::cout << "Removed meeting:" << std::endl;
		removedMeeting->print();
		delete removedMeeting;
	}
	else
	{
		std::cout << "Didn't find meeting to remove" << std::endl;
	}
}

void FindMeeting(IN OUT DayCalender& calender)
{
	std::cout << "Enter start time of meeting to find: ";
	DayCalender::TimeType startTime;
	std::cin >> startTime;

	const Meeting* const removedMeeting = calender.find(startTime);
	if (NULL != removedMeeting)
	{
		std::cout << "Found meeting:" << std::endl;
		removedMeeting->print();
	}
	else
	{
		std::cout << "Didn't find meeting" << std::endl;
	}
}

void PrintCalender(IN OUT DayCalender& calender)
{
	std::cout << "Calender:" << std::endl;
	calender.print();
}

static const Command commands[] = {
	{ AddMeeting, "Add meeting" },
	{ RemoveMeeting, "Remove meeting" },
	{ FindMeeting, "Find meeting" },
	{ PrintCalender, "Print calender" },
};

static void printMenu()
{
	std::cout << std::endl << "Enter your choice : " << std::endl;
	for (unsigned int i = 0; i < ARRAYSIZE(commands); ++i)
	{
		std::cout << (i + 1) << " - " << commands[i].description << std::endl;
	}
	std::cout << (ARRAYSIZE(commands)+1) << " (Or any other integer) - quit" << std::endl << std::endl;
}

static const Command* const getRequestedCommand()
{
	printMenu();

	const unsigned int commandIndex = GetIntegerFromUser(std::cout, std::cin);

	if ((1 <= commandIndex) && (ARRAYSIZE(commands) >= commandIndex))
	{
		return &commands[commandIndex - 1];
	}
	else
	{
		return NULL;
	}
}

int main(IN const unsigned int argc, IN const char* const argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	DayCalender calender;
	const Command* command = getRequestedCommand();
	while (NULL != command)
	{
		try
		{
			command->handler(calender);
		}
		catch (const std::exception& e)
		{
			std::cout << "Exception was thrown while performing operation on calender." << std::endl;
			std::cout << "Exception message: " << e.what() << std::endl;
		}
		command = getRequestedCommand();
	};

	return 0;
}
