
#ifndef DAY_CALENDER_INTERNALS_HEADER
#define DAY_CALENDER_INTERNALS_HEADER

template <typename TimeType>
DayCalender_t<TimeType>::DayCalender_t() :
	m_meetings()
{
	// Nothing to do here
}

template <typename TimeType>
DayCalender_t<TimeType>::~DayCalender_t()
{
	// Nothing to do here
}

template <typename TimeType>
void DayCalender_t<TimeType>::add(IN MeetingType* const meeting)
{
	if (doesHaveMeetingAtTheSameTime(*meeting))
	{
		throw std::runtime_error("There is another meeting scheduled at the same time");
	}

	// Add the new meeting at the appropriate place in array to preserve it being sorted by start time
	for (std::vector<MeetingType*>::const_iterator it = m_meetings.begin(); m_meetings.end() != it; ++it)
	{
		if (meeting->getStartTime() <= (*it)->getStartTime())
		{
			m_meetings.insert(it, meeting);
			return;
		}
	}

	// If we reached here, then the new meeting should be added to the end of the vector
	m_meetings.push_back(meeting);
}

template <typename TimeType>
bool DayCalender_t<TimeType>::doesHaveMeetingAtTheSameTime(IN const MeetingType& meeting) const
{
	for (std::vector<MeetingType*>::const_iterator it = m_meetings.begin(); m_meetings.end() != it; ++it)
	{
		if (meeting == *(*it))
		{
			return true;
		}
	}

	return false;
}

template <typename TimeType>
typename DayCalender_t<TimeType>::MeetingType* const DayCalender_t<TimeType>::remove(IN const TimeType& startTime)
{
	const std::vector<MeetingType*>::const_iterator it = findIterator(startTime);
	if (m_meetings.end() != it)
	{
		MeetingType* const meeting = *it;
		m_meetings.erase(it);
		return meeting;
	}
	else
	{
		return NULL;
	}
}

template <typename TimeType>
typename DayCalender_t<TimeType>::MeetingType* const DayCalender_t<TimeType>::find(IN const TimeType& startTime) const
{
	const std::vector<MeetingType*>::const_iterator it = findIterator(startTime);
	return ((m_meetings.end() != it) ? (*it) : (NULL));
}

template <typename TimeType>
typename std::vector<typename DayCalender_t<TimeType>::MeetingType*>::const_iterator DayCalender_t<TimeType>::findIterator(IN const TimeType& startTime) const
{
	for (std::vector<MeetingType*>::const_iterator it = m_meetings.begin(); m_meetings.end() != it; ++it)
	{
		if ((*it)->getStartTime() == startTime)
		{
			return it;
		}
	}

	return m_meetings.end();
}

template <typename TimeType>
void DayCalender_t<TimeType>::print() const
{
	unsigned int counter = 1;
	for (std::vector<MeetingType*>::const_iterator it = m_meetings.begin(); m_meetings.end() != it; ++it, ++counter)
	{
		std::cout << "Meeting " << counter << ":" << std::endl;
		(*it)->print();
		std::cout << std::endl;
	}
}

#endif
