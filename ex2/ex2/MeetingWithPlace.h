
#ifndef MEETING_WITH_PLACE_HEADER
#define MEETING_WITH_PLACE_HEADER

#include "Basic.h"
#include "Meeting.h"

template <typename TimeType>
class MeetingWithPlace_t : public Meeting_t<TimeType>
{

public:

	inline MeetingWithPlace_t(
		IN const std::string& subject, 
		IN const TimeType& startTime, 
		IN const TimeType& endTime, 
		IN const std::string& place);

	virtual ~MeetingWithPlace_t();

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(MeetingWithPlace_t);

	inline const std::string& getPlace() const;

	virtual void print() const;

private:
	std::string m_place;

};

#include "MeetingWithPlace_internals.h"

#endif
