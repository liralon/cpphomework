
#ifndef DAY_CALENDER_HEADER
#define DAY_CALENDER_HEADER

#include "Basic.h"
#include "Meeting.h"

template <typename TimeType>
class DayCalender_t
{

public:

	typedef typename TimeType TimeType;
	typedef typename Meeting_t<TimeType> MeetingType;

	inline DayCalender_t();

	virtual ~DayCalender_t();

	void add(IN MeetingType* const meeting);

	MeetingType* const remove(IN const TimeType& startTime);

	MeetingType* const find(IN const TimeType& startTime) const;

	virtual void print() const;

protected:

	typename std::vector<MeetingType*>::const_iterator findIterator(IN const TimeType& startTime) const;

	bool doesHaveMeetingAtTheSameTime(IN const MeetingType& meeting) const;

private:
	
	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(DayCalender_t);

	// Meetings sorted by start time
	// Note - pointers are stored to allow polymorphisms
	std::vector<MeetingType*> m_meetings;

};

#include "DayCalender_internals.h"

#endif
