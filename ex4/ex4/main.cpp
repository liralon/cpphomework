
#include "Basic.h"

// Note - Change the template parameters according to what you want to test
typedef int ObjectType;
#define STL_CONTAINER	std::vector

// Note - Decide if to declare TEMPLATE_TEMPLATE or not - according to what you want to test
#define TEMPLATE_TEMPLATE
#include "TemplateContainer.h"
#ifdef TEMPLATE_TEMPLATE
	typedef tContainer_t<ObjectType, STL_CONTAINER> ContainerType;
#else
	typedef tContainer_t<ObjectType, STL_CONTAINER<ObjectType*>> ContainerType;
#endif

typedef void (*CommandHandler)(IN OUT ContainerType& container);

struct Command
{
	CommandHandler handler;
	std::string description;
};

static unsigned int GetIntegerFromUser()
{
	unsigned int integer = 0;
	std::cin >> integer;
	while (!std::cin.good())
	{
		std::cout << "Invalid integer. Please retry:" << std::endl;
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cin >> integer;
	}

	return integer;
}

void PrintIsEmpty(IN OUT ContainerType& container)
{
	if (container.isEmpty())
	{
		std::cout << "Container is empty." << std::endl;
	}
	else
	{
		std::cout << "Container is not empty" << std::endl;
	}
}

void PrintSize(IN OUT ContainerType& container)
{
	std::cout << "Number of elements is: " << container.getSize() << std::endl;
}

ObjectType* const GetObjectFromUser()
{
	ObjectType* obj = new ObjectType;
	std::cout << "Enter object value: ";
	std::cin >> *obj;

	return obj;
}

void InsertAtEnd(IN OUT ContainerType& container)
{
	ObjectType* const obj = GetObjectFromUser();
	container.insertAtEnd(obj);

	std::cout << "Object inserted successfully." << std::endl;
}

void PrintFirst(IN OUT ContainerType& container)
{
	const ObjectType* const obj = container.getFirst();
	std::cout << "First object is: " << *obj << std::endl;
}

void PrintLast(IN OUT ContainerType& container)
{
	const ObjectType* const obj = container.getLast();
	std::cout << "Last object is: " << *obj << std::endl;
}

void FindElement(IN OUT ContainerType& container)
{
	const ObjectType* const objToFind = GetObjectFromUser();
	const ObjectType* const foundObj = container.find(*objToFind);
	if (NULL == foundObj)
	{
		std::cout << "Object not found." << std::endl;
	}
	else
	{
		std::cout << "Object found: " << *foundObj << std::endl;
	}
	delete objToFind;
}

void RemoveElement(IN OUT ContainerType& container)
{
	const ObjectType* const objToRemove = GetObjectFromUser();
	const ObjectType* const removedObject = container.remove(*objToRemove);
	if (NULL == removedObject)
	{
		std::cout << "Object not found." << std::endl;
	}
	else
	{
		std::cout << "Object removed: " << *removedObject << std::endl;
		delete removedObject;
	}
	delete objToRemove;
}

void RemoveAll(IN OUT ContainerType& container)
{
	container.removeAll();
	std::cout << "Removed all successfully." << std::endl;
}

void RemoveAndDeleteElement(IN OUT ContainerType& container)
{
	const ObjectType* const objToRemove = GetObjectFromUser();
	container.removeAndDelete(*objToRemove);
	std::cout << "Successfully removed and delete element." << std::endl;
	delete objToRemove;
}

void RemoveAndDeleteAll(IN OUT ContainerType& container)
{
	container.removeAndDeleteAll();
	std::cout << "Successfully removed and delete all." << std::endl;
}

void GetElementAtIndex(IN OUT ContainerType& container)
{
	std::cout << "Enter index: ";
	const size_t index = GetIntegerFromUser();

	const ObjectType* const obj = container[index];
	std::cout << "Object at index " << index << " is: " << *obj << std::endl;
}

void SetElementAtIndex(IN OUT ContainerType& container)
{
	std::cout << "Enter index: ";
	const size_t index = GetIntegerFromUser();

	ObjectType* const obj = GetObjectFromUser();

	container[index] = obj;
	std::cout << "Successfully put " << *obj << " at index " << index << std::endl;
}

static const Command commands[] = {
	{ PrintIsEmpty, "Is Empty?" },
	{ PrintSize, "Print size" },
	{ InsertAtEnd, "Insert at end" },
	{ PrintFirst, "Print first" },
	{ PrintLast, "Print last" },
	{ FindElement, "Find Element" },
	{ RemoveElement, "Remove Element" },
	{ RemoveAll, "Remove All" },
	{ RemoveAndDeleteElement, "Remove And Delete Element" },
	{ RemoveAndDeleteAll, "Remove And Delete All" },
	{ GetElementAtIndex, "Get Element At Index" },
	{ SetElementAtIndex, "Set Element At Index" },
};

static void printMenu()
{
	std::cout << std::endl << "Enter your choice : " << std::endl;
	for (unsigned int i = 0; i < ARRAYSIZE(commands); ++i)
	{
		std::cout << (i + 1) << " - " << commands[i].description << std::endl;
	}
	std::cout << (ARRAYSIZE(commands) + 1) << " (Or any other integer) - quit" << std::endl << std::endl;
}

static const Command* const getRequestedCommand()
{
	printMenu();
	const unsigned int commandIndex = GetIntegerFromUser();

	if ((1 <= commandIndex) && (ARRAYSIZE(commands) >= commandIndex))
	{
		return &commands[commandIndex - 1];
	}
	else
	{
		return NULL;
	}
}

int main(IN unsigned int argc, IN const char* const argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	ContainerType container;
	const Command* command = getRequestedCommand();
	while (NULL != command)
	{
		try
		{
			command->handler(container);
		}
		catch (const std::exception& e)
		{
			std::cout << "Exception was thrown while performing operation on container." << std::endl;
			std::cout << "Exception message: " << e.what() << std::endl;
		}
		command = getRequestedCommand();
	};

	return 0;
}
