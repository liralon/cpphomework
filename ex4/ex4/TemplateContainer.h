
#ifndef TEMPLATE_CONTAINER_HEADER
#define TEMPLATE_CONTAINER_HEADER

#include "Basic.h"

// Define macros used to define class template parameters differently based on configuration (whether to use template-template parameter or not)
#ifdef TEMPLATE_TEMPLATE
	#define CLASS_TEMPLATE_DECLEERATION		template <typename T, template<class = T*,class = std::allocator<T*>> class Container>
	#define CONTAINER_TYPE_DECLERATION		typedef typename Container<T*, std::allocator<T*>> ContainerType;
	#define IMPL_TEMPLATE_DECLERATION		template <typename T, template<class, class> class Container>
#else
	#define CLASS_TEMPLATE_DECLEERATION		template <typename T, class Container>
	#define CONTAINER_TYPE_DECLERATION		typedef Container ContainerType;
	#define IMPL_TEMPLATE_DECLERATION		template <typename T, class Container>
#endif

CLASS_TEMPLATE_DECLEERATION
class tContainer_t
{

public:

	inline tContainer_t();

	virtual ~tContainer_t();

	inline bool isEmpty() const;

	inline size_t getSize() const;

	inline void insertAtEnd(IN T* const value);

	inline T* const getFirst() const;
	inline T* const getLast() const;

	// Returns pointer to found element or NULL if not found
	T* const find(IN const T& value) const;

	// Returns pointer to found element or NULL if not found
	T* const remove(IN const T& value);
	inline void removeAll();
	inline void removeAndDelete(IN const T& value);
	void removeAndDeleteAll();

	T*& operator[](IN size_t index);
	T* const operator[](IN size_t index) const;

private:

	CONTAINER_TYPE_DECLERATION;
	typedef typename ContainerType::iterator IteratorType;
	typedef typename ContainerType::const_iterator ConstIteratorType;

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(tContainer_t);
	
	typename ConstIteratorType findIterator(IN const T& value) const;

	ContainerType m_container;

};

#include "TemplateContainer_impl.h"

#endif
