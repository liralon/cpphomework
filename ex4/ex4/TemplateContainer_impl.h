
#include "Predicates.h"

IMPL_TEMPLATE_DECLERATION
tContainer_t<T, Container>::tContainer_t() :
	m_container()
{
	// Nothing to do here
}

IMPL_TEMPLATE_DECLERATION
tContainer_t<T, Container>::~tContainer_t()
{
	// Nothing to do here
}

IMPL_TEMPLATE_DECLERATION
bool tContainer_t<T, Container>::isEmpty() const
{
	return m_container.empty();
}

IMPL_TEMPLATE_DECLERATION
size_t tContainer_t<T, Container>::getSize() const
{
	return m_container.size();
}

IMPL_TEMPLATE_DECLERATION
void tContainer_t<T, Container>::insertAtEnd(IN T* const value)
{
	m_container.push_back(value);
}

IMPL_TEMPLATE_DECLERATION
T* const tContainer_t<T, Container>::getFirst() const
{
	// Note - Calling front() method with empty container leads to undefined behaviour (according to documentation). 
	// Note - Therefore, protect it ourself
	if (isEmpty())
	{
		throw std::out_of_range("Cannot fetch first element of empty container");
	}

	return m_container.front();
}

IMPL_TEMPLATE_DECLERATION
T* const tContainer_t<T, Container>::getLast() const
{
	// Note - Calling last() method with empty container leads to undefined behaviour (according to documentation). 
	// Note - Therefore, protect it ourself
	if (isEmpty())
	{
		throw std::out_of_range("Cannot fetch last element of empty container");
	}

	return m_container.back();
}

IMPL_TEMPLATE_DECLERATION
T* const tContainer_t<T, Container>::find(IN const T& value) const
{
	const ConstIteratorType it = findIterator(value);
	return ((m_container.cend() != it) ? (*it) : (NULL));
}

IMPL_TEMPLATE_DECLERATION
T* const tContainer_t<T, Container>::remove(IN const T& value)
{
	const ConstIteratorType it = findIterator(value);
	if (m_container.cend() != it)
	{
		T* const element = *it;
		m_container.erase(it);
		return element;
	}
	else
	{
		return NULL;
	}
}

IMPL_TEMPLATE_DECLERATION
void tContainer_t<T, Container>::removeAll()
{
	m_container.clear();
}

IMPL_TEMPLATE_DECLERATION
void tContainer_t<T, Container>::removeAndDelete(IN const T& value)
{
	const T* const element = remove(value);
	if (NULL != element)
	{
		delete element;
	}
}

IMPL_TEMPLATE_DECLERATION
void tContainer_t<T, Container>::removeAndDeleteAll()
{
	while (!m_container.empty())
	{
		const ConstIteratorType firstElementIt = m_container.cbegin();
		const T* const element = *firstElementIt;
		m_container.erase(firstElementIt);
		delete element;
	}
}

IMPL_TEMPLATE_DECLERATION
T*& tContainer_t<T, Container>::operator[](IN const size_t index)
{
	if (getSize() <= index)
	{
		throw std::range_error("Index out of bounds");
	}
	
	// Note - Use of "dynamic_cast" would have been preffered (to also support classes which inherit from std::vector)
	// Note - However, we use here "typeid" operator because of exercise requirements (and the fact that STL containers doesn't support inheritance very well)
	if (typeid(m_container) == typeid(std::vector<T*>))
	{
		// Note - A pointer cast is done to avoid invoking a copy-constructor
		std::vector<T*>* containerAsVector = (std::vector<T*>*)(&m_container);
		return (*containerAsVector)[index];
	}
	else
	{
		IteratorType it = m_container.begin();
		std::advance(it, index);
		return *it;
	}
}

IMPL_TEMPLATE_DECLERATION
T* const tContainer_t<T, Container>::operator[](IN const size_t index) const
{
	// Note - This is the "Effective C++" book recommended way of avoiding code-duplication because of different constness versions of same method
	return const_cast<tContainer_t<T, Container>*>(this)->operator[](index);
}

IMPL_TEMPLATE_DECLERATION
typename tContainer_t<T, Container>::ConstIteratorType tContainer_t<T, Container>::findIterator(IN const T& value) const
{
	return std::find_if(m_container.cbegin(), m_container.cend(), std::bind2nd(PointerEqualityPredicate<T>(), value));
}
