
#ifndef PREDICATES_HEADER
#define PREDICATES_HEADER

#include "Basic.h"

template <typename T>
struct PointerEqualityPredicate : public std::binary_function<T*, T, bool>
{
	inline bool operator()(IN const T* const pointer, IN const T& value) const
	{
		return (*pointer == value);
	}
};

#endif
