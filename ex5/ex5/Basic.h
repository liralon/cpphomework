
#ifndef BASIC_HEADER
#define BASIC_HEADER

// Define some macros for better readability of code (Used to decorate function's parameters)
#define IN 
#define OUT 
#define OPTIONAL

// Used to determine the size of an array that the compiler knows it's size
#define ARRAYSIZE(arr)	(sizeof(arr) / sizeof(arr[0]))

// Used to avoid compiler warnings about unreferenced parameters
#define UNREFERENCED_PARAMETER(parameterName)	(void)(parameterName)

// Visual Studio 2013 and above support static_assert which can be used to specify conditions which must be med in compile-time
#if (1800 <= _MSC_VER)
	#define		STATIC_ASSERT(constantExpression, errorMessage)		static_assert(constantExpression, errorMessage);
#else
	#define		STATIC_ASSERT(constantExpression, errorMessage)
#endif

// Useful macro for disabling default copy constructor & assignment operator
#define DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(ClassName)		\
	ClassName(IN const ClassName&);		\
	ClassName& operator=(IN const ClassName&);

// Note - This is declared to avoid warnings of using CRT functions which are not-secure and have Microsoft's replacements
// Note - This is because in C++ course, we are required to use normal CRT functions which are not Microsoft's extensions...
#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>

#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <time.h>

#endif
