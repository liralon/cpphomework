
#ifndef TIME_UTILS_HEADER
#define TIME_UTILS_HEADER

#include "Basic.h"

namespace TimeUtils
{

	inline void GetCurrentTime(OUT tm& timeInfo)
	{
		const time_t currentTimeInElapsedSeconds = time(0);
		if ((-1) == currentTimeInElapsedSeconds)
		{
			throw std::runtime_error("Failed to get current time in elapsed seconds");
		}

		const tm* const currentTime = localtime(&currentTimeInElapsedSeconds);
		if (NULL == currentTime)
		{
			throw std::runtime_error("Failed to convert current time to local time");
		}

		timeInfo = *currentTime;
	}

	inline void NormalizeTime(OUT tm& timeInfo)
	{
		const time_t returnValue = mktime(&timeInfo);
		if ((-1) == returnValue)
		{
			throw std::runtime_error("Failed to normalize time structure");
		}
	}

}

#endif
