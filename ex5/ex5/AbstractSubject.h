
#ifndef SUBJECT_HEADER
#define SUBJECT_HEADER

#include "Basic.h"
#include "ObserverInterface.h"

class AbstractSubject
{

public:

	AbstractSubject();

	// Note - Declaring abstract virtual destructor is done to force class being abstract
	virtual ~AbstractSubject() = 0;

	virtual void attach(IN ObserverInterface* const observer);

	virtual void detach(IN ObserverInterface* const observer);

	virtual void notify() const;

private:

	std::vector<ObserverInterface*> m_observers;

};

#endif
