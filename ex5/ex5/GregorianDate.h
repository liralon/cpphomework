
#ifndef GREGORIAN_DATE_HEADER
#define GREGORIAN_DATE_HEADER

#include "Basic.h"
#include "Date.h"

class cGregorianDate_t : public cDate_t
{

public:

	// Initializes to current date
	cGregorianDate_t();

	cGregorianDate_t(IN unsigned int day, IN unsigned int month, IN unsigned int year);

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(cGregorianDate_t);

	virtual ~cGregorianDate_t();

	virtual void set(IN unsigned int day, IN unsigned int month, IN unsigned int year);

	enum PrintFormats
	{
		FORMAT_ENG_MONTH = 1,
		FORMAT_DIGITS_EURO = 2,
		FORMAT_DIGITS_AMERICAN = 3,
	};

	virtual void print(IN unsigned int format) const;

	virtual unsigned int getDayOfWeek() const;
	virtual unsigned int getDayOfYear() const;

	virtual bool isLeapYear() const;

	virtual const std::string& getNameOfDay() const;
	virtual const std::string& getNameOfMonth() const;

protected:

	virtual void advanceToNextDay();

private:

	tm getDateAsTm() const;

	// This function assumes that "tm" is already normalized
	void set(IN const tm& timeInfo);

	static const std::string NAMES_OF_DAYS[];
	static const std::string NAMES_OF_MONTHS[];

};

#endif
