
#include "Basic.h"

#include "Time.h"
#include "GregorianDate.h"

// Note - Change this to the date type you want to test
typedef cGregorianDate_t DateType;
typedef cTime_t TimeType;

typedef void(*CommandHandler)(IN OUT DateType& date, IN OUT TimeType& time);

struct Command
{
	CommandHandler handler;
	std::string description;
};

static unsigned int GetIntegerFromUser()
{
	unsigned int integer = 0;
	std::cin >> integer;
	while (!std::cin.good())
	{
		std::cout << "Invalid integer. Please retry:" << std::endl;
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cin >> integer;
	}

	return integer;
}

void GetTimeInfoFromUser(OUT unsigned int& hours, OUT unsigned int& minutes, OUT unsigned int& seconds)
{
	std::cout << "Enter hours: ";
	hours = GetIntegerFromUser();

	std::cout << "Enter minutes: ";
	minutes = GetIntegerFromUser();

	std::cout << "Enter seconds: ";
	seconds = GetIntegerFromUser();
}

void SetDate(IN OUT DateType& date, IN OUT TimeType& time)
{
	UNREFERENCED_PARAMETER(time);

	std::cout << "Enter day: ";
	const unsigned int day = GetIntegerFromUser();

	std::cout << "Enter month: ";
	const unsigned int month = GetIntegerFromUser();

	std::cout << "Enter year: ";
	const unsigned int year = GetIntegerFromUser();

	date.set(day, month, year);

	std::cout << "Date set successfully." << std::endl;
}

void PrintDate(IN OUT DateType& date, IN OUT TimeType& time)
{
	UNREFERENCED_PARAMETER(time);

	std::cout << "Choose print format: ";
	const unsigned int format = GetIntegerFromUser();

	date.print(format);
	std::cout << std::endl;
}

void PrintDateInfo(IN OUT DateType& date, IN OUT TimeType& time)
{
	UNREFERENCED_PARAMETER(time);

	std::cout << "Day: " << date.getDayOfWeek() << std::endl;
	std::cout << "Month: " << date.getMonth() << std::endl;
	std::cout << "Year: " << date.getYear() << std::endl;

	std::cout << "Day Of Month: " << date.getDayOfMonth() << std::endl;
	std::cout << "Day Of Year: " << date.getDayOfYear() << std::endl;

	std::cout << "Is leap year: " << date.isLeapYear() << std::endl;

	std::cout << "Name of day: " << date.getNameOfDay() << std::endl;
	std::cout << "Name of month: " << date.getNameOfMonth() << std::endl;
}

void SetTime(IN OUT DateType& date, IN OUT TimeType& time)
{
	UNREFERENCED_PARAMETER(date);

	unsigned int hours = 0;
	unsigned int minutes = 0;
	unsigned int seconds = 0;
	GetTimeInfoFromUser(hours, minutes, seconds);

	time.set(hours, minutes, seconds);
	std::cout << "Time set successfully." << std::endl;
}

void PrintTime(IN OUT DateType& date, IN OUT TimeType& time)
{
	UNREFERENCED_PARAMETER(date);

	std::cout << "Enter format: ";
	const TimeType::PrintFormats format = (TimeType::PrintFormats)GetIntegerFromUser();

	time.print(format);
	std::cout << std::endl;
}

void PrintTimeInfo(IN OUT DateType& date, IN OUT TimeType& time)
{
	UNREFERENCED_PARAMETER(date);

	std::cout << "Hours: " << time.getHours() << std::endl;
	std::cout << "Minutes: " << time.getMinutes() << std::endl;
	std::cout << "Seconds: " << time.getSeconds() << std::endl;
}

void AddTime(IN OUT DateType& date, IN OUT TimeType& time)
{
	UNREFERENCED_PARAMETER(date);

	unsigned int hours = 0;
	unsigned int minutes = 0;
	unsigned int seconds = 0;
	GetTimeInfoFromUser(hours, minutes, seconds);
	const cTime_t secondTime(hours, minutes, seconds);

	time += secondTime;
	std::cout << "Time added successfully." << std::endl;
}

void AttachDateToTime(IN OUT DateType& date, IN OUT TimeType& time)
{
	time.attach(&date);
	std::cout << "Date attached to time successfully." << std::endl;
}

void DetachDateToTime(IN OUT DateType& date, IN OUT TimeType& time)
{
	time.detach(&date);
	std::cout << "Date detached to time successfully." << std::endl;
}

static const Command commands[] = {
	{ SetDate, "Set Date" },
	{ PrintDate, "Print Date" },
	{ PrintDateInfo, "Print Date Info" },
	{ SetTime, "Set Time" },
	{ PrintTime, "Print Time" },
	{ PrintTimeInfo, "Print Time Info" },
	{ AddTime, "Add Time" },
	{ AttachDateToTime, "Attach date to time" },
	{ DetachDateToTime, "Detach date to time" },
};

static void printMenu()
{
	std::cout << std::endl << "Enter your choice : " << std::endl;
	for (unsigned int i = 0; i < ARRAYSIZE(commands); ++i)
	{
		std::cout << (i + 1) << " - " << commands[i].description << std::endl;
	}
	std::cout << (ARRAYSIZE(commands) + 1) << " (Or any other integer) - quit" << std::endl << std::endl;
}

static const Command* const getRequestedCommand()
{
	printMenu();
	const unsigned int commandIndex = GetIntegerFromUser();

	if ((1 <= commandIndex) && (ARRAYSIZE(commands) >= commandIndex))
	{
		return &commands[commandIndex - 1];
	}
	else
	{
		return NULL;
	}
}

int main(IN unsigned int argc, IN const char* const argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	DateType date;
	TimeType time;
	const Command* command = getRequestedCommand();
	while (NULL != command)
	{
		try
		{
			command->handler(date, time);
		}
		catch (const std::exception& e)
		{
			std::cout << "Exception was thrown while performing operation on date." << std::endl;
			std::cout << "Exception message: " << e.what() << std::endl;
		}
		command = getRequestedCommand();
	};

	return 0;
}
