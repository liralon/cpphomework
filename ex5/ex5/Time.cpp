
#include "Time.h"

cTime_t::cTime_t() :
	m_hours(0),
	m_minutes(0),
	m_seconds(0)
{
	tm timeInfo = { 0 };
	TimeUtils::GetCurrentTime(timeInfo);

	set(timeInfo);
}

cTime_t::cTime_t(IN const unsigned int hours, IN const unsigned int minutes, IN const unsigned int seconds) :
	m_hours(0),
	m_minutes(0),
	m_seconds(0)
{
	set(hours, minutes, seconds);
}

cTime_t::cTime_t(IN const cTime_t& other) :
	m_hours(other.m_hours),
	m_minutes(other.m_minutes),
	m_seconds(other.m_seconds)
{
	// Nothing to do here
}

cTime_t& cTime_t::operator=(IN const cTime_t& other)
{
	m_hours = other.m_hours;
	m_minutes = other.m_minutes;
	m_seconds = other.m_seconds;

	return *this;
}

cTime_t::~cTime_t()
{
	// Nothing to do here
}

void cTime_t::set(IN const unsigned int hours, IN const unsigned int minutes, IN const unsigned int seconds)
{
	if (hours >= 24)
	{
		throw std::out_of_range("Hours must be between 0 and 23 (including)");
	}
	if (minutes >= 60)
	{
		throw std::out_of_range("Minutes must be between 0 and 59 (including)");
	}
	if (seconds >= 60)
	{
		throw std::out_of_range("Seconds must be between 0 and 59 (including)");
	}

	m_seconds = seconds;
	m_minutes = minutes;
	m_hours = hours;
}

void cTime_t::print(IN const PrintFormats format) const
{
	switch (format)
	{
	case FORMAT_24HOURS:
		printf("%02u:%02u:%02u", getHours(), getMinutes(), getSeconds());
		break;

	case FORMAT_AMPM:
		{
			unsigned int printedHours = 0;
			std::string timeSuffix;
			const unsigned int hours = getHours();
			if (hours < 12)
			{
				printedHours = hours;
				timeSuffix = "AM";
			}
			else
			{
				printedHours = (hours == 12) ? (hours) : (hours - 12);
				timeSuffix = "PM";
			}
			printf("%02u:%02u:%02u %s", printedHours, getMinutes(), getSeconds(), timeSuffix.c_str());
		}
		break;

	default:
		// Note - should never happen
		throw std::invalid_argument("Invalid print format");
	}
}

unsigned int cTime_t::getHours() const
{
	return m_hours;
}

unsigned int cTime_t::getMinutes() const
{
	return m_minutes;
}

unsigned int cTime_t::getSeconds() const
{
	return m_seconds;
}

void cTime_t::set(IN const tm& timeInfo)
{
	m_hours = timeInfo.tm_hour;
	m_minutes = timeInfo.tm_min;
	m_seconds = timeInfo.tm_sec;
}

cTime_t& cTime_t::operator+=(IN const cTime_t& other)
{
	const cTime_t originalThis = *this;

	tm timeInfo = { 0 };
	TimeUtils::GetCurrentTime(timeInfo);
	timeInfo.tm_hour = getHours() + other.getHours();
	timeInfo.tm_min = getMinutes() + other.getMinutes();
	timeInfo.tm_sec = getSeconds() + other.getSeconds();
	TimeUtils::NormalizeTime(timeInfo);
	set(timeInfo);

	if (*this < originalThis)
	{
		notify();
	}

	return *this;
}

cTime_t operator+(IN const cTime_t& first, IN const cTime_t& second)
{
	cTime_t newTime = cTime_t(first);
	newTime += second;
	return newTime;
}

bool operator<(IN const cTime_t& first, IN const cTime_t& second)
{
	if (first.getHours() < second.getHours())
	{
		return true;
	}
	else if (first.getHours() == second.getHours())
	{
		if (first.getMinutes() < second.getMinutes())
		{
			return true;
		}
		else if (first.getMinutes() == second.getMinutes())
		{
			if (first.getSeconds() < second.getSeconds())
			{
				return true;
			}
		}
	}

	return false;
}

bool operator>(IN const cTime_t& first, IN const cTime_t& second)
{
	return !((first < second) && (first == second));
}

bool operator<=(IN const cTime_t& first, IN const cTime_t& second)
{
	return !(first > second);
}

bool operator>=(IN const cTime_t& first, IN const cTime_t& second)
{
	return !(first < second);
}

bool operator==(IN const cTime_t& first, IN const cTime_t& second)
{
	return ((first.getHours() == second.getHours()) && (first.getMinutes() == second.getMinutes()) && (first.getSeconds() == second.getSeconds()));
}

bool operator!=(IN const cTime_t& first, IN const cTime_t& second)
{
	return !(first == second);
}
