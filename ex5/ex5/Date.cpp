
#include "Date.h"
#include "Time.h"

cDate_t::cDate_t(IN unsigned int day, IN unsigned int month, IN unsigned int year) :
	m_day(day),
	m_month(month),
	m_year(year)
{
	// Nothing to do here
}

cDate_t::cDate_t(IN const cDate_t& other) :
	m_day(other.m_day),
	m_month(other.m_month),
	m_year(other.m_year)
{
	// Nothing to do here
}

cDate_t& cDate_t::operator=(IN const cDate_t& other)
{
	m_day = other.m_day;
	m_month = other.m_month;
	m_year = other.m_year;

	return *this;
}

cDate_t::~cDate_t()
{
	// Nothing to do here
}

void cDate_t::set(IN const unsigned int day, IN const unsigned int month, IN const unsigned int year)
{
	m_day = day;
	m_month = month;
	m_year = year;
}

void cDate_t::update(IN const AbstractSubject* const subject)
{
	// Update here just advances the date by one day with no specific handling for the specific time_t object that notified it
	UNREFERENCED_PARAMETER(subject);

	advanceToNextDay();
}

unsigned int cDate_t::getDayOfMonth() const
{
	return m_day;
}

unsigned int cDate_t::getMonth() const
{
	return m_month;
}

unsigned int cDate_t::getYear() const
{
	return m_year;
}
