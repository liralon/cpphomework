
#include "AbstractSubject.h"

AbstractSubject::AbstractSubject() :
	m_observers()
{
	// Nothing to do here
}

AbstractSubject::~AbstractSubject()
{
	// Nothing to do here
}

void AbstractSubject::attach(IN ObserverInterface* const observer)
{
	m_observers.push_back(observer);
}

void AbstractSubject::detach(IN ObserverInterface* const observer)
{
	std::vector<ObserverInterface*>::const_iterator it = std::find(m_observers.begin(), m_observers.end(), observer);
	if (m_observers.end() != it)
	{
		m_observers.erase(it);
	}
}

void AbstractSubject::notify() const
{
	for (std::vector<ObserverInterface*>::const_iterator it = m_observers.begin(); m_observers.end() != it; ++it)
	{
		(*it)->update(this);
	}
}
