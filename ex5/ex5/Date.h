
#ifndef DATE_HEADER
#define DATE_HEADER

#include "Basic.h"
#include "ObserverInterface.h"
#include "TimeUtils.h"

class cDate_t : public ObserverInterface
{

public:

	cDate_t(IN unsigned int day, IN unsigned int month, IN unsigned int year);

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(cDate_t);

	virtual ~cDate_t();

	virtual void set(IN unsigned int day, IN unsigned int month, IN unsigned int year);

	virtual void print(IN unsigned int format) const = 0;
	
	virtual unsigned int getDayOfMonth() const;
	virtual unsigned int getMonth() const;
	virtual unsigned int getYear() const;
	
	virtual unsigned int getDayOfWeek() const = 0;
	virtual unsigned int getDayOfYear() const = 0;
	
	virtual bool isLeapYear() const = 0;

	virtual const std::string& getNameOfDay() const = 0;
	virtual const std::string& getNameOfMonth() const = 0;

	virtual void update(IN const AbstractSubject* const subject);

protected:

	virtual void advanceToNextDay() = 0;

private:

	unsigned int m_day;		// day of month
	unsigned int m_month;
	unsigned int m_year;
};

#endif
