
#ifndef TIME_HEADER
#define TIME_HEADER

#include "Basic.h"
#include "AbstractSubject.h"
#include "TimeUtils.h"

class cTime_t : public AbstractSubject
{

public:

	// Initializes to current time
	cTime_t();

	cTime_t(IN unsigned int hours, IN unsigned int minutes, IN unsigned int seconds);

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(cTime_t);

	virtual ~cTime_t();

	void set(IN unsigned int hours, IN unsigned int minutes, IN unsigned int seconds);

	enum PrintFormats
	{
		FORMAT_24HOURS = 1,
		FORMAT_AMPM = 2,
	};

	void print(IN PrintFormats format) const;

	unsigned int getHours() const;
	unsigned int getMinutes() const;
	unsigned int getSeconds() const;

	cTime_t& operator+=(IN const cTime_t& other);

private:

	// This function assumes that "tm" is already normalized
	void set(IN const tm& timeInfo);

	unsigned int m_hours;
	unsigned int m_minutes;
	unsigned int m_seconds;	
};

cTime_t operator+(IN const cTime_t& first, IN const cTime_t& second);

bool operator<(IN const cTime_t& first, IN const cTime_t& second);
bool operator>(IN const cTime_t& first, IN const cTime_t& second);
bool operator<=(IN const cTime_t& first, IN const cTime_t& second);
bool operator>=(IN const cTime_t& first, IN const cTime_t& second);
bool operator==(IN const cTime_t& first, IN const cTime_t& second);
bool operator!=(IN const cTime_t& first, IN const cTime_t& second);

#endif
