
#include "GregorianDate.h"

const std::string cGregorianDate_t::NAMES_OF_DAYS[] = {
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
};

const std::string cGregorianDate_t::NAMES_OF_MONTHS[] = {
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December",
};

cGregorianDate_t::cGregorianDate_t() :
	cDate_t(0, 0, 0)
{
	tm timeInfo = { 0 };
	TimeUtils::GetCurrentTime(timeInfo);
	set(timeInfo);
}

cGregorianDate_t::cGregorianDate_t(IN const unsigned int day, IN const unsigned int month, IN const unsigned int year) :
	cDate_t(day, month, year)
{
	set(day, month, year);
}

cGregorianDate_t::cGregorianDate_t(IN const cGregorianDate_t& other) :
	cDate_t(other)
{
	// Nothing to do here
}

cGregorianDate_t& cGregorianDate_t::operator=(IN const cGregorianDate_t& other)
{
	cDate_t::operator=(other);
	return *this;
}

cGregorianDate_t::~cGregorianDate_t()
{
	// Nothing to do here
}

void cGregorianDate_t::set(IN const unsigned int day, IN const unsigned int month, IN const unsigned int year)
{
	if ((0 == day) || (day > 31))
	{
		throw std::out_of_range("Day must be between 1 and 31 (including)");
	}
	if ((0 == month) || (month > 12))
	{
		throw std::out_of_range("Month must be between 1 and 12 (including)");
	}
	if (year < 1900)
	{
		throw std::out_of_range("Year must be at least 1900 (including)");
	}

	cDate_t::set(day, month, year);
}

void cGregorianDate_t::print(IN const unsigned int format) const
{
	switch (format)
	{
	case FORMAT_ENG_MONTH:
		printf("%02u/%s/%04u", getDayOfMonth(), getNameOfMonth().substr(0, 3).c_str(), getYear());
		break;

	case FORMAT_DIGITS_EURO:
		printf("%02u/%02u/%04u", getDayOfMonth(), getMonth(), getYear());
		break;

	case FORMAT_DIGITS_AMERICAN:
		printf("%02u/%02u/%04u", getMonth(), getDayOfMonth(), getYear());
		break;

	default:
		// Note - Should never happen
		throw std::invalid_argument("Invalid print format");
	}
}

unsigned int cGregorianDate_t::getDayOfWeek() const
{
	return (getDateAsTm().tm_wday + 1);
}

unsigned int cGregorianDate_t::getDayOfYear() const
{
	return (getDateAsTm().tm_yday + 1);
}

bool cGregorianDate_t::isLeapYear() const
{
	return (((getYear() % 4) == 0) && (getYear() % 100 != 0)) || ((getYear() % 400) == 0);
}

const std::string& cGregorianDate_t::getNameOfDay() const
{
	return NAMES_OF_DAYS[getDayOfWeek() - 1];
}

const std::string& cGregorianDate_t::getNameOfMonth() const
{
	return NAMES_OF_MONTHS[getMonth() - 1];
}

tm cGregorianDate_t::getDateAsTm() const
{
	tm timeInfo = { 0 };
	TimeUtils::GetCurrentTime(timeInfo);
	timeInfo.tm_mday = getDayOfMonth();
	timeInfo.tm_mon = getMonth() - 1;
	timeInfo.tm_year = getYear() - 1900;
	TimeUtils::NormalizeTime(timeInfo);

	return timeInfo;
}

void cGregorianDate_t::advanceToNextDay()
{
	tm timeInfo = getDateAsTm();
	++(timeInfo.tm_mday);
	TimeUtils::NormalizeTime(timeInfo);

	set(timeInfo);
}

void cGregorianDate_t::set(IN const tm& timeInfo)
{
	cDate_t::set(timeInfo.tm_mday, timeInfo.tm_mon + 1, timeInfo.tm_year + 1900);
}
