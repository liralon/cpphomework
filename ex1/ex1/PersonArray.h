
#ifndef PERSON_ARRAY_HEADER
#define PERSON_ARRAY_HEADER

#include "Basic.h"
#include "Person.h"

class PersonArray_t
{

public:

	PersonArray_t();

	explicit PersonArray_t(IN const unsigned int initialCapacity);

	~PersonArray_t();

	inline const unsigned int getCapacity() const			{ return m_capacity; }
	inline const unsigned int getNumberOfElements() const	{ return m_numberOfElements; }

	// Note - I could have also implemented this as subscript operator overloading but decided not to because the lecturer haven't taught exceptions yet
	// Note - (Which should be used because of out-of-bounds checks. subscript operator returns reference and therefore cannot simply return NULL in this case)
	Person_t* const get(IN const unsigned int index) const;
	// Note - set method is not implemented intentionally because it violates the requirement that the array must be dense (Can create holes...)

	// Insert to the end of the array
	bool insert(IN Person_t* const person);

	Person_t* const getFirst() const;
	Person_t* const getLast() const;

	Person_t* const find(IN const Person_t& person) const;

	// Removes the first found instance
	Person_t* const remove(IN const Person_t& person);
	// Removes & Deletes all found elements
	void removeAndDelete(IN const Person_t& person);

	inline void removeAll()			{ m_numberOfElements = 0; }
	void removeAndDeleteAll();
	
	bool append(IN const unsigned int index, IN Person_t* const person);
	bool prepend(IN const unsigned int index, IN Person_t* const person);

private:

	static const unsigned int EXPAND_VALUE = 16;
	static const unsigned int DEFAULT_INITIAL_CAPCITY = EXPAND_VALUE;
	
	// Disable copy-constructor and assignment operator to disallow to copy of PersonArray_t objects
	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(PersonArray_t);

	inline bool isEmpty() const;
	inline bool isFull() const;

	bool expandBackingStore();

	// Note - return (-1) in case of not found
	const int indexOf(IN const Person_t& person) const;

	bool insertElementAtIndex(IN const unsigned int insertIndex, IN Person_t* const person);

	Person_t** m_backingStore;
	unsigned int m_capacity;
	unsigned int m_numberOfElements;

};

#endif
