
#ifndef BASIC_HEADER
#define BASIC_HEADER

// Define some macros for better readability of code (Used to decorate function's parameters)
#define IN 
#define OUT 
#define OPTIONAL

// Used to determine the size of an array that the compiler knows it's size
#define ARRAYSIZE(arr)	(sizeof(arr) / sizeof(arr[0]))

// Used to avoid compiler warnings about unreferenced parameters
#define UNREFERENCED_PARAMETER(parameterName)	(void)(parameterName)

// Visual Studio 2013 and above support static_assert which can be used to specify conditions which must be med in compile-time
#if (1800 <= _MSC_VER)
	#define		STATIC_ASSERT(constantExpression, errorMessage)		static_assert(constantExpression, errorMessage);
#else
	#define		STATIC_ASSERT(constantExpression, errorMessage)
#endif

// Useful macro for disabling default copy constructor & assignment operator
#define DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(ClassName)		\
	ClassName(IN const ClassName&);		\
	ClassName& operator=(IN const ClassName&);

#include <string>

#include <cstdio>
#include <cstdlib>
#include <iostream>

#endif