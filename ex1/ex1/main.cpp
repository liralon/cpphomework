
#include "Basic.h"
#include "Person.h"
#include "PersonArray.h"

typedef void (*CommandHandler)(IN OUT PersonArray_t& personArray);

struct Command
{
	CommandHandler handler;
	std::string description;
};

static unsigned int GetIntegerFromUser(IN OUT std::ostream& os,  OUT std::istream& is)
{
	unsigned int integer = 0;
	is >> integer;
	while (!is.good())
	{
		os << "Invalid integer. Please retry:" << std::endl;
		is.clear();
		is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		is >> integer;
	}

	return integer;
}

std::ostream& operator<<(IN OUT std::ostream& os, IN const Person_t& person)
{
	return os << "Person's Id: " << person.getId()
		<< std::endl << "Person's name: " << person.getName()
		<< std::endl << "Person's age: " << person.getAge();
}

std::istream& operator>>(IN OUT std::istream& is, IN OUT Person_t& person)
{
	std::cout << std::endl << "Enter the person's name: ";
	std::string name;
	is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::getline(is, name);

	std::cout << "Enter the person's age: ";
	const unsigned int age = GetIntegerFromUser(std::cout, is);

	person.setName(name);
	person.setAge(age);

	return is;
}

static const unsigned int GetIndexFromUser()
{
	std::cout << std::endl << "Enter the desired index for the operation: ";
	const unsigned int index = GetIntegerFromUser(std::cout, std::cin);
	return index;
}

static void PrintCapacity(IN OUT PersonArray_t& personArray)
{
	std::cout << "Capacity is " << personArray.getCapacity();
}

static void PrintNumberOfElements(IN OUT PersonArray_t& personArray)
{
	std::cout << "Number of elements is " << personArray.getNumberOfElements();
}

static void PrintElementAtIndex(IN OUT PersonArray_t& personArray)
{
	const unsigned int index = GetIndexFromUser();
	const Person_t* const person = personArray.get(index);
	if (NULL != person)
	{
		std::cout << "Person at index " << index << " is: " << std::endl << *person;
	}
	else
	{
		std::cout << "There is no person at index: " << index;
	}
}

static void InsertNewElement(IN OUT PersonArray_t& personArray)
{
	std::cout << "Inserting a new element:";
	Person_t* const person = new Person_t();
	std::cin >> *person;

	const bool isSuccess = personArray.insert(person);
	std::cout << std::endl << ((isSuccess) ? ("Inserted element successfully") : ("Failed to insert element"));
}

static void PrintFirstElement(IN OUT PersonArray_t& personArray)
{
	const Person_t* const person = personArray.getFirst();
	if (NULL != person)
	{
		std::cout << "First element in the array is:" << std::endl << *person;
	}
	else
	{
		std::cout << "First element in array does not exists (Array is empty)";
	}
}

static void PrintLastElement(IN OUT PersonArray_t& personArray)
{
	const Person_t* const person = personArray.getLast();
	if (NULL != person)
	{
		std::cout << "Last element in the array is:" << std::endl << *person;
	}
	else
	{
		std::cout << "Last element in array does not exists (Array is empty)";
	}
}

static void FindElement(IN OUT PersonArray_t& personArray)
{
	std::cout << std::endl << "Finding element:";
	Person_t personToFind;
	std::cin >> personToFind;

	const Person_t* const person = personArray.find(personToFind);
	if (NULL != person)
	{
		std::cout << "Found Person: " << std::endl << *person;
	}
	else
	{
		std::cout << "Person not found";
	}
}

static void RemoveElement(IN OUT PersonArray_t& personArray)
{
	std::cout << std::endl << "Removing element:";
	Person_t personToRemove;
	std::cin >> personToRemove;

	const Person_t* const person = personArray.remove(personToRemove);
	if (NULL != person)
	{
		std::cout << "Removed person: " << std::endl << *person;
	}
	else
	{
		std::cout << "Didn't find person to remove";
	}
}

static void RemoveAndDeleteElement(IN OUT PersonArray_t& personArray)
{
	std::cout << std::endl << "Removing & Deleting element:";
	Person_t personToRemoveAndDelete;
	std::cin >> personToRemoveAndDelete;

	personArray.removeAndDelete(personToRemoveAndDelete);
	std::cout << std::endl << "Removed successfully (if existed)";
}

static void RemoveAll(IN OUT PersonArray_t& personArray)
{
	personArray.removeAll();
	std::cout << std::endl << "Removed all elements in array";
}

static void RemoveAndDeleteAll(IN OUT PersonArray_t& personArray)
{
	personArray.removeAndDeleteAll();
	std::cout << std::endl << "Removed and deleted all elements in array";
}

static void Append(IN OUT PersonArray_t& personArray)
{
	std::cout << std::endl << "Appending element after desired index:";
	Person_t* const person = new Person_t();
	std::cin >> *person;

	const bool isSuccess = personArray.append(GetIndexFromUser(), person);
	std::cout << std::endl << ((isSuccess) ? ("Appended element successfully") : ("Failed to append element"));
}

static void Prepend(IN OUT PersonArray_t& personArray)
{
	std::cout << std::endl << "Perpending element before desired index:";
	Person_t* const person = new Person_t();
	std::cin >> *person;

	const bool isSuccess = personArray.prepend(GetIndexFromUser(), person);
	std::cout << std::endl << ((isSuccess) ? ("Prepend element successfully") : ("Failed to prepend element"));
}

static void PrintArray(IN OUT PersonArray_t& personArray)
{
	const unsigned int numberOfElements = personArray.getNumberOfElements();
	if (0 == numberOfElements)
	{
		std::cout << std::endl << "Array is empty";
	}
	else
	{
		std::cout << std::endl << "Array elements are:";
		for (unsigned int i = 0; i < personArray.getNumberOfElements(); ++i)
		{
			const Person_t* const person = personArray.get(i);
			std::cout << std::endl << *person << std::endl;
		}
	}
}

static const Command commands[] = {
	{ PrintCapacity, "Get capacity" },
	{ PrintNumberOfElements, "Get number of elements" }, 
	{ PrintElementAtIndex, "Get element at index" },
	{ InsertNewElement, "Insert new element" }, 
	{ PrintFirstElement, "Print first element" }, 
	{ PrintLastElement, "Print last element" },
	{ FindElement, "Find element" },
	{ RemoveElement, "Remove element" },
	{ RemoveAndDeleteElement, "Remove and delete element" },
	{ RemoveAll, "Remove all elements" },
	{ RemoveAndDeleteAll, "Remove and delete all elements" },
	{ Append, "Append element" },
	{ Prepend, "Prepend element" },
	{ PrintArray, "Print all elements" },
};

static void printMenu()
{
	std::cout << std::endl << std::endl << "Enter your choice : " << std::endl;
	for (unsigned int i = 0; i < ARRAYSIZE(commands); ++i)
	{
		std::cout << (i+1) << " - " << commands[i].description << std::endl;
	}
	std::cout << "15 (Or any other integer) - quit" << std::endl << std::endl;
}

static const Command* const getRequestedCommand()
{
	printMenu();

	const unsigned int commandIndex = GetIntegerFromUser(std::cout, std::cin);

	if ((1 <= commandIndex) && (ARRAYSIZE(commands) >= commandIndex))
	{
		return &commands[commandIndex - 1];
	}
	else
	{
		return NULL;
	}
}

int main(IN const unsigned int argc, IN const char* const argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	PersonArray_t personArray;
	const Command* command = getRequestedCommand();
	while (NULL != command)
	{
		command->handler(personArray);
		command = getRequestedCommand();
	};

	return 0;
}
