
#ifndef PERSON_HEADER
#define PERSON_HEADER

#include "Basic.h"

class Person_t 
{
	
public:

	// Note - Default constructor is implemeneted to support stream operator overloading
	Person_t();

	Person_t(IN const std::string& name, IN const unsigned int age);

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(Person_t);

	~Person_t();

	inline const size_t getId() const					{ return m_id; }
	inline const std::string& getName() const			{ return m_name; }
	inline const unsigned int getAge() const			{ return m_age; }

	inline void setName(IN const std::string& name)		{ m_name = name; }
	inline void setAge(IN const unsigned int age)		{ m_age = age; }

	bool operator==(IN const Person_t& other) const;
	bool operator!=(IN const Person_t& other) const;

private:

	const size_t m_id;
	std::string m_name;
	unsigned int m_age;

	static size_t m_nextPersonId;

};

#endif
