
#include "Person.h"

size_t Person_t::m_nextPersonId = 1;

Person_t::Person_t() :
	m_id(m_nextPersonId++), 
	m_name(""),
	m_age(0)
{
	// Nothing to do here
}

Person_t::Person_t(IN const std::string& name, IN const unsigned int age) :
	m_id(m_nextPersonId++), 
	m_name(name), 
	m_age(age)
{
	// Nothing to do here
}

Person_t::Person_t(IN const Person_t& other) :
	m_id(m_nextPersonId++),
	m_name(other.m_name),
	m_age(other.m_age)
{
	// Nothing to do here
}

Person_t& Person_t::operator=(IN const Person_t& other)
{
	if (this != &other)
	{
		m_name = other.m_name;
		m_age = other.m_age;
	}

	return *this;
}

Person_t::~Person_t()
{
	// Nothing to do here
}

bool Person_t::operator==(IN const Person_t& other) const
{
	return (
		(this->getName().compare(other.getName()) == 0) &&
		(this->getAge() == other.getAge())
	);
}

bool Person_t::operator!=(IN const Person_t& other) const
{
	return !(*this == other);
}
