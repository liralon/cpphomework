
#include "PersonArray.h"

PersonArray_t::PersonArray_t() :
	PersonArray_t(DEFAULT_INITIAL_CAPCITY)
{
	// Nothing to do here
}

PersonArray_t::PersonArray_t(IN const unsigned int initialCapacity) :
	m_backingStore(new Person_t*[initialCapacity]), 
	m_capacity(initialCapacity), 
	m_numberOfElements(0)
{
	// Nothing to do here
}

PersonArray_t::~PersonArray_t()
{
	// Note - We intentionally don't "delete" the array's elements because we were not necessary given ownership on the objects themselves
	delete[] m_backingStore;
}

Person_t* const PersonArray_t::get(IN const unsigned int index) const
{
	if (index >= m_numberOfElements)
	{
		return NULL;
	}
	
	return m_backingStore[index];
}

bool PersonArray_t::insert(IN Person_t* const person)
{
	if (isFull() && (!expandBackingStore()))
	{
		return false;
	}

	m_backingStore[m_numberOfElements++] = person;
	return true;
}

Person_t* const PersonArray_t::getFirst() const
{
	if (isEmpty())
	{
		return NULL;
	}

	return m_backingStore[0];
}

Person_t* const PersonArray_t::getLast() const
{
	if (isEmpty())
	{
		return NULL;
	}

	return m_backingStore[m_numberOfElements - 1];
}

Person_t* const PersonArray_t::find(IN const Person_t& person) const
{
	const int personIndex = indexOf(person);
	return (((-1) != personIndex) ? (m_backingStore[personIndex]) : (NULL));
}

Person_t* const PersonArray_t::remove(IN const Person_t& person)
{
	const int personIndex = indexOf(person);
	if ((-1) == personIndex)
	{
		return NULL;
	}

	Person_t* const arrayPerson = m_backingStore[personIndex];

	for (unsigned int i = personIndex; i < (m_numberOfElements - 1); ++i)
	{
		m_backingStore[i] = m_backingStore[i + 1];
	}
	--m_numberOfElements;

	return arrayPerson;
}

void PersonArray_t::removeAndDelete(IN const Person_t& person)
{
	const Person_t* arrayPerson = remove(person);
	while (NULL != arrayPerson)
	{
		delete arrayPerson;
		arrayPerson = remove(person);
	}
}

void PersonArray_t::removeAndDeleteAll()
{
	for (unsigned int i = 0; i < m_numberOfElements; ++i)
	{
		const Person_t* const person = m_backingStore[i];
		delete person;
	}

	removeAll();
}

bool PersonArray_t::append(IN const unsigned int index, IN Person_t* const person)
{
	if (index >= m_numberOfElements)
	{
		return false;
	}

	return insertElementAtIndex(index + 1, person);
}

bool PersonArray_t::prepend(IN const unsigned int index, IN Person_t* const person)
{
	if (index >= m_numberOfElements)
	{
		return false;
	}

	return insertElementAtIndex(index, person);
}

inline bool PersonArray_t::isEmpty() const
{
	return (0 == m_numberOfElements);
}

inline bool PersonArray_t::isFull() const
{
	return (m_numberOfElements == m_capacity);
}

bool PersonArray_t::expandBackingStore()
{
	const unsigned int newCapacity = m_capacity + EXPAND_VALUE;
	// Check for integer overflow
	if (newCapacity <= m_capacity)
	{
		return false;
	}

	Person_t** const newBackingStore = new Person_t*[newCapacity];
	memcpy(newBackingStore, m_backingStore, m_numberOfElements * sizeof(*m_backingStore));

	delete[] m_backingStore;
	m_backingStore = newBackingStore;
	m_capacity = newCapacity;

	return true;
}

const int PersonArray_t::indexOf(IN const Person_t& person) const
{
	for (unsigned int i = 0; i < m_numberOfElements; ++i)
	{
		const Person_t* const arrayPerson = m_backingStore[i];
		if (*arrayPerson == person)
		{
			return i;
		}
	}

	return (-1);
}

bool PersonArray_t::insertElementAtIndex(IN const unsigned int insertIndex, IN Person_t* const person)
{
	// Expand backing store if needed
	if (isFull() && (!expandBackingStore()))
	{
		return false;
	}
	++m_numberOfElements;
	
	// Shift elements to the right in order to reserve one space for new element
	for (unsigned int i = m_numberOfElements - 1; i > insertIndex; --i)
	{
		m_backingStore[i] = m_backingStore[i - 1];
	}

	// put new element in place
	m_backingStore[insertIndex] = person;

	return true;
}
