
#ifndef PHONE_OWNER_HEADER
#define PHONE_OWNER_HEADER

#include "Basic.h"
#include "PhoneOwnerImpl.h"
#include "ObserverInterface.h"
#include "PhoneTypes.h"

class PhoneOwner : public ObserverInterface
{

public:

	PhoneOwner(IN PhoneTypes type, IN unsigned int id, IN const std::string& name, IN const std::string& phoneNumber);

	virtual ~PhoneOwner();

	PhoneTypes getType() const;
	unsigned int getId() const;

	const std::string& getName() const;
	const std::string& getPhoneNumber() const;

	virtual void update(IN const AbstractSubject* const subject, IN unsigned int reason);

protected:

	PhoneTypes m_type;
	PhoneOwnerImpl* m_implementation;

private:

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(PhoneOwner);

};

#endif
