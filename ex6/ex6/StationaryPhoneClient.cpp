
#include "StationaryPhoneClient.h"

StationaryPhoneClient::StationaryPhoneClient(IN const unsigned int id, IN const std::string& name, IN const std::string& phoneNumber) :
	PhoneOwnerImpl(id, name, phoneNumber)
{
	// Nothing to do here
}

StationaryPhoneClient::~StationaryPhoneClient()
{
	// Nothing to do here
}
