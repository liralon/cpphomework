
#ifndef OBSERVER_HEADER
#define OBSERVER_HEADER

#include "Basic.h"

// Note - Forward deceleration to avoid cyclic-dependency
class AbstractSubject;

class ObserverInterface
{

public:

	virtual ~ObserverInterface()	{}

	virtual void update(IN const AbstractSubject* const subject, IN unsigned int reason) = 0;

protected:

	ObserverInterface()		{}

};

#endif
