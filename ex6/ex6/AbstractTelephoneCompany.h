
#ifndef TELEPHONE_COMPANY_INTERFACE_HEADER
#define TELEPHONE_COMPANY_INTERFACE_HEADER

#include "Basic.h"
#include "AbstractSubject.h"
#include "PhoneTypes.h"
#include "PhoneOwner.h"

class AbstractTelephoneCompany : public AbstractSubject
{

public:

	enum NotifyReasons
	{
		PRICE_OF_SERVICE_UPDATED = 0,
		MOBILE_PHONES_CAN_BE_UPGRADED_UPDATED,
	};

	AbstractTelephoneCompany(IN unsigned int priceOfService, IN bool canMobilePhonesBeUpgraded);

	// Note - Destructor is declared abstract to force class being abstract
	virtual ~AbstractTelephoneCompany() = 0;

	virtual void setPriceOfService(IN unsigned int priceOfService);
	virtual void setMobilePhonesCanBeUpgraded(IN bool canMobilePhonesBeUpgraded);

	virtual unsigned int getPriceOfService() const;
	virtual bool canMobilePhonesBeUpgraded() const;

	// Utility methods to attach/detach more easily than standard generic observer interface
	void attach(IN PhoneOwner* const phoneOwner);
	void detach(IN PhoneOwner* const phoneOwner);

	virtual ObserversSet getUsersByType(IN PhoneTypes type) const;

private:

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(AbstractTelephoneCompany);

	unsigned int m_priceOfService;
	bool m_canMobilePhonesBeUpgraded;

};

#endif
