
#ifndef PHONE_OWNER_IMPL_HEADER
#define PHONE_OWNER_IMPL_HEADER

#include "Basic.h"
#include "PhoneTypes.h"
#include "ObserverInterface.h"

class PhoneOwnerImpl : public ObserverInterface
{

public:

	PhoneOwnerImpl(IN unsigned int id, IN const std::string& name, IN const std::string& phoneNumber);
	
	// Note - Destructor is declared abstract to force class being abstract
	virtual ~PhoneOwnerImpl() = 0;

	static PhoneOwnerImpl* const create(IN PhoneTypes type, IN unsigned int id, IN const std::string& name, IN const std::string& phoneNumber);

	unsigned int getId() const;
	const std::string& getName() const;
	const std::string& getPhoneNumber() const;

	virtual void update(IN const AbstractSubject* const subject, IN unsigned int reason);

private:

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(PhoneOwnerImpl);

	unsigned int m_id;
	std::string m_name;
	std::string m_phoneNumber;
};

#endif
