
#include "AbstractSubject.h"

AbstractSubject::AbstractSubject() :
m_observersByType()
{
	// Nothing to do here
}

AbstractSubject::~AbstractSubject()
{
	// Nothing to do here
}

void AbstractSubject::attach(IN const unsigned int type, IN ObserverInterface* const observer)
{
	ObserversMap::const_iterator it = m_observersByType.find(type);
	if (m_observersByType.cend() == it)
	{
		m_observersByType.insert(ObserversMap::value_type(type, ObserversSet()));
	}

	m_observersByType[type].push_back(observer);
}

void AbstractSubject::detach(IN const unsigned int type, IN ObserverInterface* const observer)
{
	ObserversSet& observers = m_observersByType[type];
	std::vector<ObserverInterface*>::const_iterator it = std::find(observers.cbegin(), observers.cend(), observer);
	if (observers.end() != it)
	{
		observers.erase(it);

		if (observers.empty())
		{
			m_observersByType.erase(type);
		}
	}
}

void AbstractSubject::notify(IN const unsigned int type, IN const unsigned int reason) const
{
	// std::map doesn't implement subscript operator for non-const object
	const ObserversSet& observers = const_cast<AbstractSubject* const>(this)->m_observersByType[type];
	notifyObservers(observers, reason);
}

void AbstractSubject::notifyAllTypes(IN const unsigned int reason) const
{
	for (ObserversMap::const_iterator it = m_observersByType.cbegin(); m_observersByType.cend() != it; ++it)
	{
		const ObserversSet& observers = it->second;
		notifyObservers(observers, reason);
	}
}

void AbstractSubject::notifyObservers(IN const std::vector<ObserverInterface*>& observers, IN const unsigned int reason) const
{
	for (ObserversSet::const_iterator it = observers.cbegin(); observers.cend() != it; ++it)
	{
		(*it)->update(this, reason);
	}
}

const AbstractSubject::ObserversMap& AbstractSubject::getAllObservers() const
{
	return m_observersByType;
}
