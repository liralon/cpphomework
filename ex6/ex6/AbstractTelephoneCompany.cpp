
#include "AbstractTelephoneCompany.h"

AbstractTelephoneCompany::AbstractTelephoneCompany(IN unsigned int priceOfService, IN bool canMobilePhonesBeUpgraded) :
	AbstractSubject(),
	m_priceOfService(priceOfService), 
	m_canMobilePhonesBeUpgraded(canMobilePhonesBeUpgraded)
{
	// Nothing to do here
}

AbstractTelephoneCompany::~AbstractTelephoneCompany()
{
	// Nothing to do here
}

void AbstractTelephoneCompany::setPriceOfService(IN unsigned int priceOfService)
{
	m_priceOfService = priceOfService;
}

void AbstractTelephoneCompany::setMobilePhonesCanBeUpgraded(IN const bool canMobilePhonesBeUpgraded)
{
	m_canMobilePhonesBeUpgraded = canMobilePhonesBeUpgraded;
}

unsigned int AbstractTelephoneCompany::getPriceOfService() const
{
	return m_priceOfService;
}

bool AbstractTelephoneCompany::canMobilePhonesBeUpgraded() const
{
	return m_canMobilePhonesBeUpgraded;
}

void AbstractTelephoneCompany::attach(IN PhoneOwner* const phoneOwner)
{
	AbstractSubject::attach(phoneOwner->getType(), phoneOwner);
}

void AbstractTelephoneCompany::detach(IN PhoneOwner* const phoneOwner)
{
	AbstractSubject::detach(phoneOwner->getType(), phoneOwner);
}

AbstractTelephoneCompany::ObserversSet AbstractTelephoneCompany::getUsersByType(IN const PhoneTypes type) const
{
	const ObserversMap& observersByType = getAllObservers();
	ObserversMap::const_iterator it = observersByType.find(type);
	return ((observersByType.cend() != it) ? (it->second) : (ObserversSet()));
}
