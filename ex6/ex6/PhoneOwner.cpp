
#include "PhoneOwner.h"

PhoneOwner::PhoneOwner(IN PhoneTypes type, IN const unsigned int id, IN const std::string& name, IN const std::string& phoneNumber) :
	m_type(type),
	m_implementation(PhoneOwnerImpl::create(type, id, name, phoneNumber))
{
	// Nothing to do here
}

PhoneOwner::~PhoneOwner()
{
	delete m_implementation;
}

PhoneTypes PhoneOwner::getType() const
{
	return m_type;
}

unsigned int PhoneOwner::getId() const
{
	return m_implementation->getId();
}

const std::string& PhoneOwner::getName() const
{
	return m_implementation->getName();
}

void PhoneOwner::update(IN const AbstractSubject* const subject, IN const unsigned int reason)
{
	m_implementation->update(subject, reason);
}
