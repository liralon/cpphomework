
#ifndef TELEPHONE_COMPANY_HEADER
#define TELEPHONE_COMPANY_HEADER

#include "Basic.h"
#include "AbstractTelephoneCompany.h"

class TelephoneCompany : public AbstractTelephoneCompany
{

public:

	static TelephoneCompany& instance();

	virtual ~TelephoneCompany();

	virtual void setPriceOfService(IN unsigned int priceOfService);
	virtual void setMobilePhonesCanBeUpgraded(IN bool canMobilePhonesBeUpgraded);

private:

	static const unsigned int DEFAULT_PRICE_OF_SERVICE = 100;

	// Note - This class is Singleton and therefore constructor is private
	TelephoneCompany();

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(TelephoneCompany);

};

#endif
