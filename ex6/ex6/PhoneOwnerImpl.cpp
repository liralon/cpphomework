
#include "PhoneOwnerImpl.h"
#include "MobilePhoneClient.h"
#include "StationaryPhoneClient.h"
#include "AbstractTelephoneCompany.h"

PhoneOwnerImpl::PhoneOwnerImpl(IN const unsigned int id, IN const std::string& name, IN const std::string& phoneNumber) :
	m_id(id), 
	m_name(name),
	m_phoneNumber(phoneNumber)
{
	// Nothing to do here
}

PhoneOwnerImpl::~PhoneOwnerImpl()
{
	// Nothing to do here
}

PhoneOwnerImpl* const PhoneOwnerImpl::create(IN PhoneTypes type, IN const unsigned int id, IN const std::string& name, IN const std::string& phoneNumber)
{
	switch (type)
	{
	case MOBILE_PHONE:
		return new MobilePhoneClient(id, name, phoneNumber);
		break;

	case STATIONARY_PHONE:
		return new StationaryPhoneClient(id, name, phoneNumber);
		break;

	default:
		// Note - Should never get to here
		throw std::invalid_argument("Requested to create an invalid phone type");
	}
}

unsigned int PhoneOwnerImpl::getId() const
{
	return m_id;
}

const std::string& PhoneOwnerImpl::getName() const
{
	return m_name;
}

const std::string& PhoneOwnerImpl::getPhoneNumber() const
{
	return m_phoneNumber;
}

void PhoneOwnerImpl::update(IN const AbstractSubject* const subject, IN const unsigned int reason)
{
	const AbstractTelephoneCompany* const telephoneCompany = reinterpret_cast<const AbstractTelephoneCompany* const>(subject);
	if (AbstractTelephoneCompany::PRICE_OF_SERVICE_UPDATED == reason)
	{
		printf("Client (id: %u, name: %s, phone number: %s) has noticed that price of service was changed to: %u\n",
			m_id, m_name.c_str(), m_phoneNumber.c_str(), telephoneCompany->getPriceOfService());
	}
}
