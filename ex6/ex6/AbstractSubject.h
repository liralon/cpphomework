
#ifndef SUBJECT_HEADER
#define SUBJECT_HEADER

#include "Basic.h"
#include "ObserverInterface.h"

class AbstractSubject
{

public:

	typedef std::vector<ObserverInterface*> ObserversSet;

	AbstractSubject();

	// Note - Declaring abstract virtual destructor is done to force class being abstract
	virtual ~AbstractSubject() = 0;

	virtual void attach(IN unsigned int type, IN ObserverInterface* const observer);

	virtual void detach(IN unsigned int type, IN ObserverInterface* const observer);

	virtual void notify(IN unsigned int type, IN unsigned int reason) const;

	virtual void notifyAllTypes(IN unsigned int reason) const;

protected:
	
	typedef std::map<unsigned int, ObserversSet> ObserversMap;

	const ObserversMap& getAllObservers() const;

private:

	void notifyObservers(IN const ObserversSet& observers, IN unsigned int reason) const;

	ObserversMap m_observersByType;
	
};

#endif
