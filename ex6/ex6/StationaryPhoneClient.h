
#ifndef STATIONARY_PHONE_CLIENT_HEADER
#define STATIONARY_PHONE_CLIENT_HEADER

#include "Basic.h"
#include "PhoneOwnerImpl.h"

class StationaryPhoneClient : public PhoneOwnerImpl
{

public:

	StationaryPhoneClient(IN const unsigned int id, IN const std::string& name, IN const std::string& phoneNumber);

	virtual ~StationaryPhoneClient();

private:

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(StationaryPhoneClient);

};

#endif
