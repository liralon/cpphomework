
#include "Basic.h"

#include "PhoneOwner.h"
#include "TelephoneCompany.h"

typedef void(*CommandHandler)(IN OUT PhoneOwner& mobileOwner, IN OUT PhoneOwner& stationaryOwner);

struct Command
{
	CommandHandler handler;
	std::string description;
};

static unsigned int GetIntegerFromUser()
{
	unsigned int integer = 0;
	std::cin >> integer;
	while (!std::cin.good())
	{
		std::cout << "Invalid integer. Please retry:" << std::endl;
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cin >> integer;
	}

	return integer;
}

static bool GetBoolFromUser()
{
	unsigned int integer = 0;
	std::cin >> integer;
	while (!std::cin.good() || (integer > 1))
	{
		std::cout << "Invalid choice. Please retry:" << std::endl;
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cin >> integer;
	}

	return (integer != 0);
}

void SetPrice(IN OUT PhoneOwner& mobileOwner, IN OUT PhoneOwner& stationaryOwner)
{
	UNREFERENCED_PARAMETER(mobileOwner);
	UNREFERENCED_PARAMETER(stationaryOwner);

	std::cout << "Choose the new price:" << std::endl;
	const unsigned int priceOfService = GetIntegerFromUser();
	TelephoneCompany::instance().setPriceOfService(priceOfService);
	std::cout << "New price set to " << priceOfService << std::endl;
}

void SetUpgradeStatus(IN OUT PhoneOwner& mobileOwner, IN OUT PhoneOwner& stationaryOwner)
{
	UNREFERENCED_PARAMETER(mobileOwner);
	UNREFERENCED_PARAMETER(stationaryOwner);

	std::cout << "Choose upgrade status: 0 - not upgradeable, 1 - upgradeable" << std::endl;
	const bool isUpgradeable = GetBoolFromUser();
	TelephoneCompany::instance().setMobilePhonesCanBeUpgraded(isUpgradeable);
	const std::string& boolString = isUpgradeable ? "true" : "false";
	std::cout << "Upgrade status set to " << boolString << std::endl;
}

void AttachMobileOwner(IN OUT PhoneOwner& mobileOwner, IN OUT PhoneOwner& stationaryOwner)
{
	UNREFERENCED_PARAMETER(stationaryOwner);

	TelephoneCompany::instance().attach(&mobileOwner);
	std::cout << "Attached mobile owner to telephone company." << std::endl;
}

void DetachMobileOwner(IN OUT PhoneOwner& mobileOwner, IN OUT PhoneOwner& stationaryOwner)
{
	UNREFERENCED_PARAMETER(stationaryOwner);

	TelephoneCompany::instance().detach(&mobileOwner);
	std::cout << "Mobile owner detached from telephone company successfully." << std::endl;
}

void AttachStationaryOwner(IN OUT PhoneOwner& mobileOwner, IN OUT PhoneOwner& stationaryOwner)
{
	UNREFERENCED_PARAMETER(mobileOwner);

	TelephoneCompany::instance().attach(&stationaryOwner);
	std::cout << "Attached stationary owner to telephone company." << std::endl;
}

void DetachStationaryOwner(IN OUT PhoneOwner& mobileOwner, IN OUT PhoneOwner& stationaryOwner)
{
	UNREFERENCED_PARAMETER(mobileOwner);

	TelephoneCompany::instance().detach(&stationaryOwner);
	std::cout << "Stationary owner detached from telephone company successfully." << std::endl;
}

static const Command commands[] = {
	{ SetPrice, "Set new price for the telephone company service" },
	{ SetUpgradeStatus, "Set upgrade status" },
	{ AttachMobileOwner, "Attach mobile owner to telephone company" },
	{ DetachMobileOwner, "Detach mobile owner from telephone company" },
	{ AttachStationaryOwner, "Attach stationary owner to telephone company" },
	{ DetachStationaryOwner, "Detach stationary owner from telephone company" }
};

static void printMenu()
{
	std::cout << std::endl << "Enter your choice : " << std::endl;
	for (unsigned int i = 0; i < ARRAYSIZE(commands); ++i)
	{
		std::cout << (i + 1) << " - " << commands[i].description << std::endl;
	}
	std::cout << (ARRAYSIZE(commands) + 1) << " (Or any other integer) - quit" << std::endl << std::endl;
}

static const Command* const getRequestedCommand()
{
	printMenu();
	const unsigned int commandIndex = GetIntegerFromUser();

	if ((1 <= commandIndex) && (ARRAYSIZE(commands) >= commandIndex))
	{
		return &commands[commandIndex - 1];
	}
	else
	{
		return NULL;
	}
}

int main(IN unsigned int argc, IN const char* const argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	PhoneOwner mobileOwner(MOBILE_PHONE, 1, "john", "050-5555555");
	PhoneOwner stationaryOwner(STATIONARY_PHONE, 2, "mike", "050-6666666");

	const Command* command = getRequestedCommand();
	while (NULL != command)
	{
		try
		{
			command->handler(stationaryOwner, mobileOwner);
		}
		catch (const std::exception& e)
		{
			std::cout << "Exception was thrown." << std::endl;
			std::cout << "Exception message: " << e.what() << std::endl;
		}
		command = getRequestedCommand();
	};

	return 0;
}
