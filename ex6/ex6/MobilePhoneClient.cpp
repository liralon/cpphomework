
#include "MobilePhoneClient.h"
#include "AbstractTelephoneCompany.h"

MobilePhoneClient::MobilePhoneClient(IN const unsigned int id, IN const std::string& name, IN const std::string& phoneNumber) :
	PhoneOwnerImpl(id, name, phoneNumber)
{
	// Nothing to do here
}

MobilePhoneClient::~MobilePhoneClient()
{
	// Nothing to do here
}

void MobilePhoneClient::update(IN const AbstractSubject* const subject, IN const unsigned int reason)
{
	PhoneOwnerImpl::update(subject, reason);

	const AbstractTelephoneCompany* const telephoneCompany = reinterpret_cast<const AbstractTelephoneCompany* const>(subject);
	if (AbstractTelephoneCompany::MOBILE_PHONES_CAN_BE_UPGRADED_UPDATED == reason)
	{
		printf("Mobile client (id: %u, name: %s, phone number: %s) noticed that mobile can %sbe upgraded\n",
			getId(), getName().c_str(), getPhoneNumber().c_str(), (telephoneCompany->canMobilePhonesBeUpgraded()) ? ("") : ("not "));
	}
}
