
#ifndef MOBILE_PHONE_CLIENT_HEADER
#define MOBILE_PHONE_CLIENT_HEADER

#include "Basic.h"
#include "PhoneOwnerImpl.h"

class MobilePhoneClient : public PhoneOwnerImpl
{

public:

	MobilePhoneClient(IN const unsigned int id, IN const std::string& name, IN const std::string& phoneNumber);

	virtual ~MobilePhoneClient();

	virtual void update(IN const AbstractSubject* const subject, IN unsigned int reason);

private:

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(MobilePhoneClient);

};

#endif
