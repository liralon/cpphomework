
#include "TelephoneCompany.h"

TelephoneCompany& TelephoneCompany::instance()
{
	// Note - Mayers singleton design:
	// Note - Will cause the object to be initialized (constructor to be called) only on first call to the instance() method
	static TelephoneCompany instance;
	return instance;
}

TelephoneCompany::~TelephoneCompany()
{
	// Nothing to do here
}

void TelephoneCompany::setPriceOfService(IN const unsigned int priceOfService)
{
	AbstractTelephoneCompany::setPriceOfService(priceOfService);
	notifyAllTypes(NotifyReasons::PRICE_OF_SERVICE_UPDATED);
}

void TelephoneCompany::setMobilePhonesCanBeUpgraded(IN const bool canMobilePhonesBeUpgraded)
{
	AbstractTelephoneCompany::setMobilePhonesCanBeUpgraded(canMobilePhonesBeUpgraded);
	notify(MOBILE_PHONE, NotifyReasons::MOBILE_PHONES_CAN_BE_UPGRADED_UPDATED);
}

TelephoneCompany::TelephoneCompany() :
	AbstractTelephoneCompany(DEFAULT_PRICE_OF_SERVICE, false)
{
	// Nothing to do here
}
