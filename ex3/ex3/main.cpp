
#include "Basic.h"
#include "asciiIO.h"
#include "binIO.h"

typedef void(*CommandHandler)(IN OUT virtIO_t& io);

struct Command
{
	CommandHandler handler;
	std::string description;
};

static std::string GetStringFromUser(OUT std::istream& is)
{
	std::string inputStr;
	std::getline(is, inputStr);

	return inputStr;
}

static unsigned int GetIntegerFromUser(IN OUT std::ostream& os, OUT std::istream& is)
{
	unsigned int integer = 0;
	is >> integer;
	while (!is.good())
	{
		os << "Invalid integer. Please retry:" << std::endl;
		is.clear();
		is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		is >> integer;
	}

	return integer;
}

static void GetPath(IN OUT virtIO_t& io)
{
	std::cout << "Path is: " << io.getPath() << std::endl;
}

static void GetMode(IN OUT virtIO_t& io)
{
	std::cout << "Mode is: " << io.getMode() << std::endl;
}

static void GetSize(IN OUT virtIO_t& io)
{
	std::cout << "File length is: " << io.getSize() << std::endl;
}

static void GetStatus(IN OUT virtIO_t& io)
{
	std::cout << "Status is: " << io.getStatus() << std::endl;
}

static void WriteBuffer(IN OUT virtIO_t& io)
{
	std::cout << "Enter buffer size: ";
	const unsigned int size = GetIntegerFromUser(std::cout, std::cin);

	unsigned char* buffer = new unsigned char[size];
	for (unsigned int i = 0; i < size; ++i) {
		std::cout << "Enter byte " << (i + 1) << ": ";
		std::cin >> buffer[i];
	}

	io << buffer, size;
	
	if (NULL != buffer)
	{
		delete[] buffer;
		buffer = NULL;
	}
}

static void ReadBuffer(IN OUT virtIO_t& io)
{
	std::cout << "Enter buffer size: ";
	const unsigned int size = GetIntegerFromUser(std::cout, std::cin);

	unsigned char* buffer = new unsigned char[size];
	io >> buffer, size;

	std::cout << "Read buffer content:" << std::endl;
	for (unsigned int i = 0; i < size; ++i)
	{
		std::cout << "Byte " << (i + 1) << ": " << buffer[i] << std::endl;
	}
	
	if (NULL != buffer)
	{
		delete[] buffer;
		buffer = NULL;
	}
}

static void WriteChar(IN OUT virtIO_t& io)
{
	char obj = 0;
	std::cout << "Enter char to write: ";
	std::cin >> obj;

	io << obj;
}

static void ReadChar(IN OUT virtIO_t& io)
{
	char obj = 0;
	io >> obj;

	std::cout << "Read char: " << obj;
}

static void WriteUnsignedChar(IN OUT virtIO_t& io)
{
	unsigned char obj = 0;
	std::cout << "Enter unsigned char to write: ";
	std::cin >> obj;

	io << obj;
}

static void ReadUnsignedChar(IN OUT virtIO_t& io)
{
	char obj = 0;
	io >> obj;

	std::cout << "Read unsigned char: " << obj;
}

static void WriteShort(IN OUT virtIO_t& io)
{
	short obj = 0;
	std::cout << "Enter short to write: ";
	std::cin >> obj;

	io << obj;
}

static void ReadShort(IN OUT virtIO_t& io)
{
	short obj = 0;
	io >> obj;

	std::cout << "Read short: " << obj;
}

static void WriteUnsignedShort(IN OUT virtIO_t& io)
{
	unsigned short obj = 0;
	std::cout << "Enter unsigned short to write: ";
	std::cin >> obj;

	io << obj;
}

static void ReadUnsignedShort(IN OUT virtIO_t& io)
{
	unsigned short obj = 0;
	io >> obj;

	std::cout << "Read unsigned short: " << obj;
}

static void WriteInt(IN OUT virtIO_t& io)
{
	int obj = 0;
	std::cout << "Enter int to write: ";
	std::cin >> obj;

	io << obj;
}

static void ReadInt(IN OUT virtIO_t& io)
{
	int obj = 0;
	io >> obj;

	std::cout << "Read int: " << obj;
}

static void WriteUnsignedInt(IN OUT virtIO_t& io)
{
	unsigned int obj = 0;
	std::cout << "Enter unsigned int to write: ";
	std::cin >> obj;

	io << obj;
}

static void ReadUnsignedInt(IN OUT virtIO_t& io)
{
	unsigned int obj = 0;
	io >> obj;

	std::cout << "Read unsigned int: " << obj;
}

static void WriteLong(IN OUT virtIO_t& io)
{
	long obj = 0;
	std::cout << "Enter long to write: ";
	std::cin >> obj;

	io << obj;
}

static void ReadLong(IN OUT virtIO_t& io)
{
	long obj = 0;
	io >> obj;

	std::cout << "Read long: " << obj;
}

static void WriteUnsignedLong(IN OUT virtIO_t& io)
{
	unsigned long obj = 0;
	std::cout << "Enter unsigned long to write: ";
	std::cin >> obj;

	io << obj;
}

static void ReadUnsignedLong(IN OUT virtIO_t& io)
{
	unsigned long obj = 0;
	io >> obj;

	std::cout << "Read unsigned long: " << obj;
}

static void WriteFloat(IN OUT virtIO_t& io)
{
	float obj = 0;
	std::cout << "Enter float to write: ";
	std::cin >> obj;

	io << obj;
}

static void ReadFloat(IN OUT virtIO_t& io)
{
	float obj = 0;
	io >> obj;

	std::cout << "Read float: " << obj;
}

static void WriteDouble(IN OUT virtIO_t& io)
{
	double obj = 0;
	std::cout << "Enter double to write: ";
	std::cin >> obj;

	io << obj;
}

static void ReadDouble(IN OUT virtIO_t& io)
{
	double obj = 0;
	io >> obj;

	std::cout << "Read double: " << obj;
}

static const Command commands[] = {
	{ GetPath, "Get Path" },
	{ GetMode, "Get Mode" },
	{ GetSize, "Get Size" },
	{ GetStatus, "Get Status" }, 

	{ WriteBuffer, "Write Buffer" },
	{ ReadBuffer, "Read Buffer" },

	{ WriteChar, "Write char" },
	{ ReadChar, "Read char" },

	{ WriteUnsignedChar, "Write unsigned char" },
	{ ReadUnsignedChar, "Read unsigned char" },

	{ WriteShort, "Write short" },
	{ ReadShort, "Read short" },

	{ WriteUnsignedShort, "Write unsigned short" },
	{ ReadUnsignedShort, "Read unsigned short" },

	{ WriteInt, "Write int" },
	{ ReadInt, "Read int" },

	{ WriteUnsignedInt, "Write unsigned int" },
	{ ReadUnsignedInt, "Read unsigned int" },

	{ WriteLong, "Write long" },
	{ ReadLong, "Read long" },

	{ WriteUnsignedLong, "Write unsigned long" },
	{ ReadUnsignedLong, "Read unsigned long" },

	{ WriteFloat, "Write float" },
	{ ReadFloat, "Read float" },

	{ WriteDouble, "Write double" },
	{ ReadDouble, "Read double" },
};

static void PrintMenu()
{
	std::cout << std::endl << "Enter your choice : " << std::endl;
	for (unsigned int i = 0; i < ARRAYSIZE(commands); ++i)
	{
		std::cout << (i + 1) << " - " << commands[i].description << std::endl;
	}
	std::cout << (ARRAYSIZE(commands) + 1) << " (Or any other integer) - quit" << std::endl << std::endl;
}

static const Command* const GetRequestedCommand()
{
	PrintMenu();

	std::cin.clear();
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	const unsigned int commandIndex = GetIntegerFromUser(std::cout, std::cin);

	if ((1 <= commandIndex) && (ARRAYSIZE(commands) >= commandIndex))
	{
		return &commands[commandIndex - 1];
	}
	else
	{
		return NULL;
	}
}

static virtIO_t* AllocateVirtIO()
{
	std::cout << "Select I/O backing store (0 for standard i/o, any other integer for file): ";
	const bool isStandardIO = (0 == GetIntegerFromUser(std::cout, std::cin));

	std::string path;
	std::string mode;
	if (!isStandardIO)
	{
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		std::cout << "Enter path: ";
		path = GetStringFromUser(std::cin);

		std::cout << "Enter mode: ";
		mode = GetStringFromUser(std::cin);
	}

	std::cout << "Select the I/O type (0 for binary, any other integer for ASCII): ";
	const bool isBinary = (0 == GetIntegerFromUser(std::cout, std::cin));
	if (isBinary)
	{
		return ((isStandardIO) ? (new binIO_t()) : (new binIO_t(path, mode)));
	}
	else
	{
		return ((isStandardIO) ? (new asciiIO_t()) : (new asciiIO_t(path, mode)));
	}
}

int main(IN const unsigned int argc, IN const char* const argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	virtIO_t* io = AllocateVirtIO();
	const Command* command = GetRequestedCommand();
	while (NULL != command)
	{
		try
		{
			command->handler(*io);
		}
		catch (const std::exception& e)
		{
			std::cout << "Exception was thrown while performing operation on I/O." << std::endl;
			std::cout << "Exception message: " << e.what() << std::endl;
		}
		command = GetRequestedCommand();
	};
	
	if (NULL != io)
	{
		delete io;
		io = NULL;
	}

	return 0;
}
