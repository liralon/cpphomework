
#ifndef BIN_IO_HEADER
#define BIN_IO_HEADER

#include "Basic.h"
#include "virtIO.h"

class binIO_t : public virtIO_t
{

public:

	inline binIO_t() :
		virtIO_t()
	{
		// Nothing to do here
	}

	inline binIO_t(IN const std::string& path, IN const std::string& mode) :
		virtIO_t(path, mode + "b")
	{
		// Nothing to do here
	}

	virtual ~binIO_t();

	virtual virtIO_t& operator>>(OUT char& obj);
	virtual virtIO_t& operator<<(IN char obj);

	virtual virtIO_t& operator>>(OUT unsigned char& obj);
	virtual virtIO_t& operator<<(IN unsigned char obj);

	virtual virtIO_t& operator>>(OUT short& obj);
	virtual virtIO_t& operator<<(IN short obj);

	virtual virtIO_t& operator>>(OUT unsigned short& obj);
	virtual virtIO_t& operator<<(IN unsigned short obj);

	virtual virtIO_t& operator>>(OUT int& obj);
	virtual virtIO_t& operator<<(IN int obj);

	virtual virtIO_t& operator>>(OUT unsigned int& obj);
	virtual virtIO_t& operator<<(IN unsigned int obj);

	virtual virtIO_t& operator>>(OUT long& obj);
	virtual virtIO_t& operator<<(IN long obj);

	virtual virtIO_t& operator>>(OUT unsigned long& obj);
	virtual virtIO_t& operator<<(IN unsigned long obj);

	virtual virtIO_t& operator>>(OUT float& obj);
	virtual virtIO_t& operator<<(IN float obj);

	virtual virtIO_t& operator>>(OUT double& obj);
	virtual virtIO_t& operator<<(IN double obj);

private:

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(binIO_t);

};

#endif
