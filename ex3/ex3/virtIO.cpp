
#include "virtIO.h"

virtIO_t::virtIO_t(IN const std::string& path, IN const std::string& mode) :
	m_inputStream(NULL), 
	m_outputStream(NULL), 

	m_readPosition(0), 
	m_writePosition(0), 

	m_path(path), 
	m_mode(mode), 

	m_status(ok_e), 

	m_currentReadBuffer(NULL),
	m_currentWriteBuffer(NULL)
{
	m_inputStream = fopen(path.c_str(), mode.c_str());
	if (NULL == m_inputStream)
	{
		setStatus(cant_open_file_e);
		goto cleanup;
	}
	
	m_outputStream = m_inputStream;

	// In case we are appending, we should set write position to end of output stream
	if (std::string::npos != mode.find('a'))
	{
		m_writePosition = getSize();
	}

cleanup:
	if (ok_e != m_status)
	{
		CloseStreams();
	}
}

virtIO_t::~virtIO_t()
{
	CloseStreams();
}

void virtIO_t::CloseStreams()
{
	// We need to call fclose only if we don't operate on standard IO streams
	if (m_outputStream != stdout)
	{
		// We operate on a file. Therefore, both input and output streams are the same and therefore only one should be closed
		if (NULL != m_inputStream)
		{
			fclose(m_inputStream);
			m_inputStream = NULL;
			m_outputStream = NULL;
		}
	}
}

size_t virtIO_t::getSize()
{
	verifySreams();

	const long int currentOffset = ftell(m_outputStream);
	if ((-1) == currentOffset)
	{
		setStatus(bad_access_e);
		throw std::runtime_error("Failed to get current output stream position");
	}

	if (0 != fseek(m_outputStream, 0, SEEK_END))
	{
		setStatus(bad_access_e);
		throw std::runtime_error("Failed to seek to output stream end");
	}

	const long int fileSize = ftell(m_outputStream);
	if ((-1) == fileSize)
	{
		setStatus(bad_access_e);
		throw std::runtime_error("Failed to ftell output stream");
	}

	if (0 != fseek(m_outputStream, currentOffset, SEEK_SET))
	{
		setStatus(bad_access_e);
		throw std::runtime_error("Failed to restore output stream position");
	}

	setStatus(ok_e);

	return fileSize;
}

void virtIO_t::setStatus(IN FileStatus status)
{
	m_status = status;
}

virtIO_t& virtIO_t::operator>>(IN OUT void* const buffer)
{
	m_currentReadBuffer = buffer;
	return *this;
}

virtIO_t& virtIO_t::operator<<(IN const void* const buffer)
{
	m_currentWriteBuffer = buffer;
	return *this;
}

void virtIO_t::operator,(IN size_t currentReadWriteBlockSize)
{
	if ((NULL != m_currentReadBuffer) && (NULL != m_currentWriteBuffer))
	{
		throw std::invalid_argument("Invalid usage of operator,. Ambiguity of specifying size for read or write");
	}

	if (NULL != m_currentReadBuffer)
	{
		// Note - We nullify the current buffer to indicate that we have processed the current read buffer
		void* const buffer = m_currentReadBuffer;
		m_currentReadBuffer = NULL;
		read(buffer, 1, currentReadWriteBlockSize);
	}
	else if (NULL != m_currentWriteBuffer)
	{
		// Note - We nullify the current buffer to indicate that we have processed the current write buffer
		const void* const buffer = m_currentWriteBuffer;
		m_currentWriteBuffer = NULL;
		write(buffer, 1, currentReadWriteBlockSize);
	}
	else
	{
		throw std::invalid_argument("Invalid usage of operator,. Must have declared buffer for read/write before using operator");
	}
}

void virtIO_t::read(IN OUT void* const buffer, IN size_t sizeOfElement, IN size_t numberOfElements)
{
	verifySreams();

	seekStreamToPosition(m_inputStream, m_readPosition);

	const size_t numberOfElementsRead = fread(buffer, sizeOfElement, numberOfElements, m_inputStream);
	if (numberOfElementsRead != numberOfElements)
	{
		setStatus(readErr_e);
		throw std::runtime_error("Failed to read elements from file");
	}
	
	m_readPosition += sizeOfElement * numberOfElements;
	
	setStatus(ok_e);
}

void virtIO_t::write(IN const void* const buffer, IN size_t sizeOfElement, IN size_t numberOfElements)
{
	verifySreams();

	seekStreamToPosition(m_outputStream, m_writePosition);

	const size_t numberOfElementsWritten = fwrite(buffer, sizeOfElement, numberOfElements, m_outputStream);
	if (numberOfElementsWritten != numberOfElements)
	{
		setStatus(readErr_e);
		throw std::runtime_error("Failed to write elements from file");
	}

	m_writePosition += sizeOfElement * numberOfElements;

	setStatus(ok_e);
}

void virtIO_t::seekStreamToPosition(IN FILE* const stream, IN size_t position)
{
	if (0 != fseek(stream, position, SEEK_SET))
	{
		setStatus(bad_access_e);
		throw std::runtime_error("Failed to set file position");
	}
}

void virtIO_t::verifySreams()
{
	// Note - It is sufficient to check only input stream. See constructors logic to understand why
	if (NULL == m_inputStream)
	{
		throw std::runtime_error("Virtual IO stream failed to initialize. Cannot operate on stream");
	}
}
