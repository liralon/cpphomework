
#ifndef VIRT_IO_HEADER
#define VIRT_IO_HEADER

#include "Basic.h"

class virtIO_t 
{

public:

	enum FileStatus
	{
		ok_e, 
		cant_open_file_e, 
		bad_access_e,
		writeErr_e, 
		readErr_e, 
	};

	inline virtIO_t() :
		m_inputStream(stdin),
		m_outputStream(stdout),

		m_readPosition(0),
		m_writePosition(0),

		m_path(),
		m_mode("w+"),

		m_status(ok_e),

		m_currentReadBuffer(NULL),
		m_currentWriteBuffer(NULL)
	{
		// Nothing to do here
	}

	virtIO_t(IN const std::string& path, IN const std::string& mode);

	virtual ~virtIO_t();

	inline const std::string& getPath() const		{ return m_path; }
	inline const std::string& getMode() const		{ return m_mode; }
	
	size_t getSize();
	
	inline FileStatus getStatus() const				{ return m_status; }

	virtual virtIO_t& operator>>(IN OUT void* const buffer);
	virtual virtIO_t& operator<<(IN const void* const buffer);
	void operator,(IN size_t currentReadWriteBlockSize);

	virtual virtIO_t& operator>>(OUT char& obj) = 0;
	virtual virtIO_t& operator<<(IN char obj) = 0;

	virtual virtIO_t& operator>>(OUT unsigned char& obj) = 0;
	virtual virtIO_t& operator<<(IN unsigned char obj) = 0;

	virtual virtIO_t& operator>>(OUT short& obj) = 0;
	virtual virtIO_t& operator<<(IN short obj) = 0;

	virtual virtIO_t& operator>>(OUT unsigned short& obj) = 0;
	virtual virtIO_t& operator<<(IN unsigned short obj) = 0;

	virtual virtIO_t& operator>>(OUT int& obj) = 0;
	virtual virtIO_t& operator<<(IN int obj) = 0;

	virtual virtIO_t& operator>>(OUT unsigned int& obj) = 0;
	virtual virtIO_t& operator<<(IN unsigned int obj) = 0;

	virtual virtIO_t& operator>>(OUT long& obj) = 0;
	virtual virtIO_t& operator<<(IN long obj) = 0;

	virtual virtIO_t& operator>>(OUT unsigned long& obj) = 0;
	virtual virtIO_t& operator<<(IN unsigned long obj) = 0;

	virtual virtIO_t& operator>>(OUT float& obj) = 0;
	virtual virtIO_t& operator<<(IN float obj) = 0;

	virtual virtIO_t& operator>>(OUT double& obj) = 0;
	virtual virtIO_t& operator<<(IN double obj) = 0;

	void read(IN OUT void* const buffer, IN size_t sizeOfElement, IN size_t numberOfElements);
	void write(IN const void* const buffer, IN size_t sizeOfElement, IN size_t numberOfElements);

protected:

	inline FILE* const getInputStream() const				{ return m_inputStream; }
	inline FILE* const getOutputStream() const				{ return m_outputStream; }
	
	inline size_t getReadPosition() const					{ return m_readPosition; }
	inline void setReadPosition(IN size_t readPosition)		{ m_readPosition = readPosition; }

	inline size_t getWritePosition() const					{ return m_writePosition; }
	inline void setWritePosition(IN size_t writePosition)	{ m_writePosition = writePosition; }

	void seekStreamToPosition(IN FILE* const stream, IN size_t position);

	virtual void setStatus(IN FileStatus status);

	void verifySreams();

private:

	void CloseStreams();

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(virtIO_t);

	FILE* m_inputStream;
	FILE* m_outputStream;

	size_t m_readPosition;
	size_t m_writePosition;

	std::string m_path;
	std::string m_mode;
	
	FileStatus m_status;
	
	const void* m_currentWriteBuffer;
	void* m_currentReadBuffer;
};

#endif
