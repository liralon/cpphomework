
#include "asciiIO.h"

asciiIO_t::~asciiIO_t()
{
	// Nothing to do here
}

virtIO_t& asciiIO_t::operator>>(OUT char& obj)
{
	read("%c", &obj);
	return *this;
}

virtIO_t& asciiIO_t::operator<<(IN char obj)
{
	write("%c", obj);
	return *this;
}

virtIO_t& asciiIO_t::operator>>(OUT unsigned char& obj)
{
	read("%hhu", &obj);
	return *this;
}

virtIO_t& asciiIO_t::operator<<(IN unsigned char obj)
{
	write("%hhu", obj);
	return *this;
}

virtIO_t& asciiIO_t::operator>>(OUT short& obj)
{
	read("%hd", &obj);
	return *this;
}

virtIO_t& asciiIO_t::operator<<(IN short obj)
{
	write("%hd", obj);
	return *this;
}

virtIO_t& asciiIO_t::operator>>(OUT unsigned short& obj)
{
	read("%hu", &obj);
	return *this;
}

virtIO_t& asciiIO_t::operator<<(IN unsigned short obj)
{
	write("%hu", obj);
	return *this;
}

virtIO_t& asciiIO_t::operator>>(OUT int& obj)
{
	read("%d", &obj);
	return *this;
}

virtIO_t& asciiIO_t::operator<<(IN int obj)
{
	write("%d", obj);
	return *this;
}

virtIO_t& asciiIO_t::operator>>(OUT unsigned int& obj)
{
	read("%u", &obj);
	return *this;
}

virtIO_t& asciiIO_t::operator<<(IN unsigned int obj)
{
	write("%u", obj);
	return *this;
}

virtIO_t& asciiIO_t::operator>>(OUT long& obj)
{
	read("%l", &obj);
	return *this;
}

virtIO_t& asciiIO_t::operator<<(IN long obj)
{
	write("%l", obj);
	return *this;
}

virtIO_t& asciiIO_t::operator>>(OUT unsigned long& obj)
{
	read("%ul", &obj);
	return *this;
}

virtIO_t& asciiIO_t::operator<<(IN unsigned long obj)
{
	write("%ul", obj);
	return *this;
}

virtIO_t& asciiIO_t::operator>>(OUT float& obj)
{
	read("%f", &obj);
	return *this;
}

virtIO_t& asciiIO_t::operator<<(IN float obj)
{
	write("%f", obj);
	return *this;
}

virtIO_t& asciiIO_t::operator>>(OUT double& obj)
{
	read("%f", &obj);
	return *this;
}

virtIO_t& asciiIO_t::operator<<(IN double obj)
{
	write("%f", obj);
	return *this;
}

void asciiIO_t::read(IN const std::string& format, OUT void* const obj)
{
	verifySreams();

	seekStreamToPosition(getInputStream(), getReadPosition());

	const int numberOfParsedElements = fscanf(getInputStream(), format.c_str(), obj);
	if (1 != numberOfParsedElements)
	{
		setStatus(readErr_e);
		throw std::runtime_error("Failed to read object from file");
	}

	const long int newReadPosition = ftell(getInputStream());
	if ((-1) == newReadPosition)
	{
		setStatus(bad_access_e);
		throw std::runtime_error("Failed to get new read position. Read position not updated");
	}
	setReadPosition(newReadPosition);

	setStatus(ok_e);
}
