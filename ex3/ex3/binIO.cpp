
#include "binIO.h"

binIO_t::~binIO_t()
{
	// Nothing to do here
}

virtIO_t& binIO_t::operator>>(OUT char& obj)
{
	read(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator<<(IN char obj)
{
	write(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator>>(OUT unsigned char& obj)
{
	read(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator<<(IN unsigned char obj)
{
	write(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator>>(OUT short& obj)
{
	read(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator<<(IN short obj)
{
	write(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator>>(OUT unsigned short& obj)
{
	read(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator<<(IN unsigned short obj)
{
	write(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator>>(OUT int& obj)
{
	read(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator<<(IN int obj)
{
	write(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator>>(IN unsigned int& obj)
{
	read(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator<<(IN unsigned int obj)
{
	write(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator>>(OUT long& obj)
{
	read(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator<<(IN long obj)
{
	write(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator>>(OUT unsigned long& obj)
{
	read(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator<<(IN unsigned long obj)
{
	write(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator>>(OUT float& obj)
{
	read(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator<<(IN float obj)
{
	write(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator>>(OUT double& obj)
{
	read(&obj, sizeof(obj), 1);
	return *this;
}

virtIO_t& binIO_t::operator<<(IN double obj)
{
	write(&obj, sizeof(obj), 1);
	return *this;
}
