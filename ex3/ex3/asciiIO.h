
#ifndef ASCII_IO_HEADER
#define ASCII_IO_HEADER

#include "Basic.h"
#include "virtIO.h"

class asciiIO_t : public virtIO_t
{

public:

	inline asciiIO_t() :
		virtIO_t()
	{
		// Nothing to do here
	}
	
	inline asciiIO_t(IN const std::string& path, IN const std::string& mode) :
		virtIO_t(path, mode)
	{
		if (std::string::npos != mode.find("b"))
		{
			setStatus(bad_access_e);
		}
	}

	virtual ~asciiIO_t();

	virtual virtIO_t& operator>>(OUT char& obj);
	virtual virtIO_t& operator<<(IN char obj);

	virtual virtIO_t& operator>>(OUT unsigned char& obj);
	virtual virtIO_t& operator<<(IN unsigned char obj);

	virtual virtIO_t& operator>>(OUT short& obj);
	virtual virtIO_t& operator<<(IN short obj);

	virtual virtIO_t& operator>>(OUT unsigned short& obj);
	virtual virtIO_t& operator<<(IN unsigned short obj);

	virtual virtIO_t& operator>>(OUT int& obj);
	virtual virtIO_t& operator<<(IN int obj);

	virtual virtIO_t& operator>>(OUT unsigned int& obj);
	virtual virtIO_t& operator<<(IN unsigned int obj);

	virtual virtIO_t& operator>>(OUT long& obj);
	virtual virtIO_t& operator<<(IN long obj);

	virtual virtIO_t& operator>>(OUT unsigned long& obj);
	virtual virtIO_t& operator<<(IN unsigned long obj);

	virtual virtIO_t& operator>>(OUT float& obj);
	virtual virtIO_t& operator<<(IN float obj);

	virtual virtIO_t& operator>>(OUT double& obj);
	virtual virtIO_t& operator<<(IN double obj);

protected:

	void read(IN const std::string& format, OUT void* const obj);

	template <typename T>
	void write(IN const std::string& format, IN const T& obj);

private:

	DECLARE_COPY_COSNTRUCTOR_AND_ASSIGNMENT_OPERATOR(asciiIO_t);

};

template <typename T>
void asciiIO_t::write(IN const std::string& format, IN const T& obj)
{
	verifySreams();

	seekStreamToPosition(getOutputStream(), getWritePosition());

	// Note - We separate the objects by whitespace in order to not merge them on read
	const int returnValue = fprintf(getOutputStream(), (format + " ").c_str(), obj);
	if (0 > returnValue)
	{
		setStatus(writeErr_e);
		throw std::runtime_error("Failed to write object to output stream");
	}

	const long int newWritePosition = ftell(getOutputStream());
	if ((-1) == newWritePosition)
	{
		setStatus(bad_access_e);
		throw std::runtime_error("Failed to get new write position. Write position not updated");
	}
	setWritePosition(newWritePosition);

	setStatus(ok_e);
}

#endif
